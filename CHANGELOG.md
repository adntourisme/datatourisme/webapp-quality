# CHANGELOG

Ce fichier est basé sur [Keep a Changelog](http://keepachangelog.com/)
et le projet utilise [Semantic Versioning](http://semver.org/).

## [Unreleased]

## [1.0.0] - 2022-05-03

# Ajout

- Notifications : rapport et alerte d'évolution d'anomalies par email

## [0.9.1] - 2022-04-07

# Modification

- Le rôle ROLE_PRODUCER devient ROLE_MANAGER (Référent)
- Les administrateurs peuvent maintenant modifier le rôles des utilisateurs

## [0.9.0] - 2022-04-05

### Ajout

- Connexion SSO à partir de la plateforme Producteur

## [0.8.0] - 2022-03-28

### Ajout

- Ajout d'un onglet avec la carte du POI dans le tiroir de l'explorer
- Explorer : les colonnes sont maintenant ré-ordonnables

## [0.7.0] - 2022-03-18

### Correction

- Correction des libellés de pagination sur la liste des utilisateurs (#33)
- Correction du texte du mail d'invitation
- Correction du texte du mail de confirmation de connexion suite à invitation

### Modification

- Ajout d'un lien cliquable sur le camembert des types anomalies vers l'explorer
- Ajout de la latitude et longitude dans l'export
- Explorer : possibilité de changer l'ordre des colonnes par glisser/déposer

## [0.6.0] - 2022-03-04

### Modification

- Migration vers Symfony 5.4

### Correction

- Correction de la commande de génération de données de carte
- Optimisation du Dockerfile
- Correction du service FileUploader

## [0.5.0] - 2022-03-01

### Ajout

- Nouvelle commande de synchronisation des créateurs depuis le SPARQL Endpoint

### Modification

- La sélection des créateurs de données est maintenant liée à une table de référence synchronisée

## [0.4.0] - 2022-03-01

### Modification

- Nouveau dashboard
- Dashboard : la carte de chaleur utilise maintenant une source Cluster

## [0.3.1] - 2022-02-22

### Ajout

- Ajout du support de Sentry
- Explorer : Support du paramètre maxresultWindow pour la génération de la pagination

### Modification

- Explorer : la fiche d'un POI s'ouvre maintenant avec un simple clic sur la colonne Nom
- Explorer : les URL présentes dans la description d'une anomalie sont maintenant cliquables
- Le fichier CSS d'OpenLayers est maintenant intégré aux assets

### Correction

- Correction de la page d'accueil des actualités

## [0.3.0] - 2022-02-18

### Ajout

- Explorer : Télécharger au format CSV/XLSX/ODT

## [0.2.2] - 2022-02-17

### Ajout

- Explorer : ajout de la facette "Nombre de média"
- Dashboard : Le 2nd niveau de type de POI est maintenant visible dans le widget répartition
- Carte : les points sont maintenant colorisés en fonction de leur premier type primaire.

### Modification

- Carte : la taille des points est maintenant linéairement liée au niveau de zoom

## [0.2.1] - 2022-02-11

### Modification

- Explorer : les POI peuvent maintenant être recherchés par identifiant
- Les producteurs peuvent maintenant impersonate leurs utilisateurs
- Le numéro de téléphone n'est plus un champ obligatoire

## [0.2.0] - 2022-02-11

### Ajout

- Ajout de la section Anomalies

### Correction

- Explorer : le tri par commune fonctionne

## [0.1.0] - 2022-02-06

### Ajout

- Ajout de la procédure d'invitation d'utilisateur
- Ajout d'un bouton sur la carte de l'explorateur pour passer en mode plein écran

### Correction

- variables_order = "EGPCS" dans php.ini de production
- Ajout du réseau datatourisme dans le docker-compose.deploy.yml
- L'adresse d'envoi des mails de notification est maintenant une variable d'environnement MAILER_SENDER
- Ajout de la variable d'environnement TRUSTED_PROXIES
- Le mot de passe doit maintenant faire au moins 14 caractères

## [0.0.1] - 2022-01-28

### Ajout

- Publication initiale
