<?php

/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace App\Elasticsearch;

use Symfony\Component\HttpFoundation\HeaderUtils;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Box\Spout\Writer\Common\Creator\WriterEntityFactory;
use Box\Spout\Writer\WriterInterface;
use Exception;

/**
 * AbstractExport
 */
abstract class AbstractExport
{
    private AbstractIndex $index;

    /**
     * Constructor
     */
    public function __construct(AbstractIndex $index)
    {
        $this->index = $index;
    }

    abstract protected function getKeyPattern(): array;

    abstract protected function getFileBaseName(): string;

    /**
     * Proxy and download file from a Request to the ES index
     * @param Request $request
     * @param string $format
     * @param string $queryString
     * @param bool $alter
     * @return Response
     */
    public function download(string $queryString, string $format = 'csv', bool $alter = true): Response
    {
        $client = $this->index->getClient();

        // overwrite PHP.ini max_execution_time to allow long downloads
        set_time_limit(0);

        $query = '{"query":' . $queryString . '}';

        // alter query from user data
        if ($alter) {
            $query = $this->index->alterQueryString($query);
        }

        // workaround for issue with "match_all" search query and "aggs"
        // see https://github.com/elastic/elasticsearch-php/issues/495
        // $query = str_replace(
        //     ['{"match_all":{}}', '"aggs":{},'],
        //     ['{"match_all":{"boost":1.0}}', ''],
        //     $query
        // );

        // params
        $duration = '2m';
        $size = 500;

        // create writer
        $writer = $this->createWriter($format);

        $response = new StreamedResponse();
        $response->setCallback(function () use ($query, $client, $duration, $size, $writer) {

            // open resource
            $writer->openToFile('php://output');

            // write headers
            $headers = $this->getHeaders();
            $writer->addRow(WriterEntityFactory::createRowFromArray($headers));

            // first scroll request
            $scrollResponse = $client->request('POST', $this->index->getIndice() . '/_search', [
                'headers' => ['Content-Type' => 'application/json'],
                'body' => $query,
                'query' => [
                    'scroll' => $duration,
                    'size' => $size
                ],
            ]);
            $scroll = $scrollResponse->toArray();
            $scrollId = $scroll['_scroll_id'];

            while (isset($scroll['hits']['hits']) && count($scroll['hits']['hits']) > 0) {
                // decode rows
                $hits = $scroll['hits']['hits'];
                foreach ($hits as $hit) {
                    $values = $this->getValues($hit['_source']);
                    $rowFromValues = WriterEntityFactory::createRowFromArray($values);
                    $writer->addRow($rowFromValues);
                }
                // next scroll
                $scrollResponse = $client->request('POST', '/_search/scroll', [
                    'json' => [
                        'scroll' => $duration,
                        'scroll_id' => $scrollId
                    ],
                ]);
                $scroll = $scrollResponse->toArray();
                $scrollId = $scroll['_scroll_id'];
            };

            $writer->close();

            if (isset($scrollId)) {
                $scrollResponse = $client->request('DELETE', '/_search/scroll', [
                    'query' => ["scroll_id" => $scrollId],
                ]);
                $scrollResponse->getContent();
            }
        });

        $disposition = HeaderUtils::makeDisposition(
            HeaderUtils::DISPOSITION_ATTACHMENT,
            $this->getFileBaseName() . '_' . date("Y-m-d_H-i-s") . '.' . $format
        );
        $response->headers->set('Content-Disposition', $disposition);
        $response->send();

        return $response;
    }

    /**
     * Create writer according to format
     */
    private function createWriter(string $format): WriterInterface
    {
        switch ($format) {
            case "csv":
                return WriterEntityFactory::createCSVWriter();
            case "xlsx":
                return WriterEntityFactory::createXLSXWriter();
            case "ods":
                return WriterEntityFactory::createODSWriter();
            default:
                throw new Exception("Unsupported format : $format");
        }
    }

    /**
     * @return string[]
     */
    private function getHeaders(): array
    {
        return array_values($this->getKeyPattern());
    }

    /**
     * @param array $source
     * @return array
     */
    private function getValues(array $source): array
    {
        $line = [];
        foreach (array_keys($this->getKeyPattern()) as $path) {
            $parts = explode(".", $path);
            $size = count($parts);
            if ($size === 1) {
                // only one key in path
                $line[] = $this->getValue($source, $parts[0]);
            } else {
                // multiple keys in path
                $item = $source;
                // intermediate keys
                for ($i = 0; $i < $size - 1; $i++) {
                    if (!$this->checkValue($item, $parts, $i)) {
                        break;
                    }
                }
                // last key
                $line[] = $this->getValue($item, $parts[$size - 1]);
            }
        }
        return $line;
    }

    /**
     * checkValue
     */
    private function checkValue(array &$item, array $parts, $key): bool
    {
        if (isset($item[$parts[$key]])) {
            $item = $item[$parts[$key]];
            if (is_array($item) && array_key_exists(0, $item)) {
                $item = $item[0];
            }
            return true;
        }
        return false;
    }

    /**
     * @param array $item
     * @param $path
     * @return mixed|string
     */
    private function getValue(array $item, $path)
    {
        if (isset($item[$path])) {
            if (is_array($item[$path])) {
                return implode("\n", $item[$path]);
            } else {
                return $item[$path];
            }
        } else {
            return '';
        }
    }
}
