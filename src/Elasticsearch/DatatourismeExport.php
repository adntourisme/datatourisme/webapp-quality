<?php

/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace App\Elasticsearch;

/**
* Datatourisme export
*/
class DatatourismeExport extends AbstractExport
{
    /**
     * download pattern
     * path => column name
     *
     * @return string[]
     */
    protected function getKeyPattern(): array
    {
        return [
            "label.@fr" => "Nom",
            "identifier" => "Identifiant",
            "analyze.type1" => "Type",
            "analyze.type2" => "Sous-type",
            "analyze.type3" => "Sous-type",
            "isLocatedAt.address.hasAddressCity.label.@fr" => "Commune",
            "isLocatedAt.address.postalCode" => "Code postal",
            "isLocatedAt.address.hasAddressCity.insee" => "Code INSEE",
            "isLocatedAt.address.hasAddressCity.isPartOfDepartment.label.@fr" => "Département",
            "isLocatedAt.address.hasAddressCity.isPartOfDepartment.isPartOfRegion.label.@fr" => "Région",
            "isLocatedAt.geo.latitude" => "Latitude",
            "isLocatedAt.geo.longitude" => "Longitude",
            "hasContact.homepage" => "Site internet",
            "hasBeenCreatedBy.legalName" => "Créateur",
            "lastUpdate" => "Mise à jour",
            "lastUpdateDatatourisme" => "Mise à jour DATAtourisme",
            "analyze.mediaCount" => "Médias",
            "analyze.anomalyStatistics.total" => "Erreurs",
        ];
    }

    /**
     * @return string
     */
    protected function getFileBaseName(): string
    {
        return 'export_poi';
    }
}
