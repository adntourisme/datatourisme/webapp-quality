<?php

/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\Exception;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Role\RoleHierarchyInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\PasswordUpgraderInterface;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository implements PasswordUpgraderInterface
{
    /** @var Security */
    private $security;
    /** @var RoleHierarchyInterface */
    private $roleHierarchy;

    public function __construct(ManagerRegistry $registry, Security $security, RoleHierarchyInterface $roleHierarchy)
    {
        parent::__construct($registry, User::class);
        $this->security = $security;
        $this->roleHierarchy = $roleHierarchy;
    }

    /**
     * Used to upgrade (rehash) the user's password automatically over time.
     */
    public function upgradePassword(PasswordAuthenticatedUserInterface $user, string $newHashedPassword): void
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', \get_class($user)));
        }

        $user->setPassword($newHashedPassword);
        $this->_em->persist($user);
        $this->_em->flush();
    }

    /**
     * @return QueryBuilder
     */
    public function getIndexQueryBuilder(): QueryBuilder
    {
        $qb = $this->createQueryBuilder('u')
            ->groupBy('u.id')
            // last login
            // ->addSelect('MAX(l.datetime) AS lastLogin')
            // ->leftJoin('AppBundle:LoginHistory', 'l', Join::WITH, 'l.user = u')
            // organisations
            // ->leftJoin(ProducerOrganization::class, 'po', Join::WITH, 'po = u.organization')
            ->leftJoin('u.producer', 'p')
            ->addGroupBy('p.id');

        if (!$this->security->isGranted('ROLE_ADMIN')) {
            /** @var User */
            $user = $this->security->getUser();
            $producer = $user->getProducer();

            if ($producer) {
                $qb->andWhere("u.producer = :producer")
                    ->setParameter('producer', $producer);
            }
        }

        return $qb;
    }

    /**
     * @param string $email
     * @param bool $isActive
     * @return User|null
     * @throws NonUniqueResultException
     */
    public function getUserByEmail(string $email, bool $isActive = true): ?User
    {
        $qb = $this->createQueryBuilder('u');
        $qb->andWhere('lower(u.email) = :email')
            ->setParameter('email', mb_strtolower($email, 'UTF-8'));
        if ($isActive) {
            $qb->andWhere($qb->expr()->eq('u.enabled', ':enabled'))->setParameter('enabled', true)
                ->andWhere($qb->expr()->eq('u.locked', ':locked'))->setParameter('locked', false);
        }

        return $qb->setMaxResults(1)->getQuery()->getOneOrNullResult();
    }

    /**
     * @param string $role
     * @param bool $strict
     * @return array|null
     */
    public function getRecipientsByRole(string $role, bool $strict = false): ?array
    {
        if ($strict) {
            $users = $this->findBy(['role' => $role]);
        } else {
            $users = array_filter($this->findAll(), function ($user) use ($role) {
                return in_array($role, $this->roleHierarchy->getReachableRoleNames($user->getRoles()));
            });
        }

        return array_map(function ($user) {
            return $user->toRecipient();
        }, $users);
    }

    /**
     * @param string $userType
     * @param bool $periodic
     * @param string|null $frequency
     * @return array|null
     */
    public function getNotificationRecipients(string $userType, bool $periodic, ?string $frequency): ?array
    {
        $qb = $this->createQueryBuilder('u');

        if ($periodic) {
            $qb->andWhere('u.periodicNotifications LIKE :pattern')
                ->setParameter('pattern', "%$frequency%");
        } else {
            $qb->andWhere('u.thresholdNotifications IS NOT NULL');
        }

        if ($userType == 'national') {
            $qb->andWhere('u.producer IS NULL');
        } elseif ($userType == 'producer') {
            $qb->andWhere('u.producer IS NOT NULL')
                ->andWhere('u.hasBeenCreatedBy IS NULL');
        } else {
            $qb->andWhere('u.producer IS NOT NULL')
                ->andWhere('u.hasBeenCreatedBy IS NOT NULL');
        }

        return $qb->getQuery()->getResult();
    }
}
