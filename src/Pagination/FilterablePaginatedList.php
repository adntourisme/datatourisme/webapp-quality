<?php

/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace App\Pagination;

use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Knp\Component\Pager\Pagination\AbstractPagination;
use Knp\Component\Pager\PaginatorInterface;
use Lexik\Bundle\FormFilterBundle\Filter\FilterBuilderUpdaterInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\HttpFoundation\Request;

class FilterablePaginatedList
{
    const QUERY_HINT_NAME = 'knp_paginator.count';

    /** @var PaginatorInterface */
    protected $paginator;

    /** @var FilterBuilderUpdaterInterface */
    protected $queryUpdater;

    /** @var AbstractPagination */
    protected $pagination;

    /** @var QueryBuilder */
    protected $queryBuilder;

    /** @var FormInterface */
    protected $filterForm;

    /** @var FormView */
    protected $form;

    /** @var int $queryHint */
    protected $queryHint;

    /** @var int $pageSize */
    protected $pageSize = 20;

    /** @var array $paginationOptions */
    protected $paginationOptions = [];

    /**
     * FilterablePaginatedList constructor.
     *
     * @param QueryBuilder|Query            $query
     * @param FormInterface                 $filterForm
     * @param PaginatorInterface            $paginator
     * @param FilterBuilderUpdaterInterface $queryUpdater
     */
    public function __construct(QueryBuilder $queryBuilder, FormInterface $filterForm, PaginatorInterface $paginator, FilterBuilderUpdaterInterface $queryUpdater)
    {
        $this->paginator = $paginator;
        $this->queryUpdater = $queryUpdater;
        $this->filterForm = $filterForm;
        $this->queryBuilder = $queryBuilder;
    }

    /**
     * @param Request $request
     * @return $this
     */
    public function handleRequest(Request $request)
    {
        if ($request->query->has($this->filterForm->getName())) {
            $this->filterForm->submit($request->query->get($this->filterForm->getName()));
            $this->queryUpdater->addFilterConditions($this->filterForm, $this->queryBuilder);
        }

        /** @var Query $query */
        $query = $this->queryBuilder->getQuery();

        if (
            !empty(array_filter($this->filterForm->getData() ?: [], function ($element) {
                return $element !== null;
            }))
        ) {
            $query->setHint(self::QUERY_HINT_NAME, count($query->getResult()));
        } elseif (null !== $this->queryHint) {
            $query->setHint(self::QUERY_HINT_NAME, $this->queryHint);
        }

        // create and handle paginator
        $this->pagination = $this->paginator->paginate(
            $query,
            $request->query->get('page', 1),
            $this->pageSize,
            $this->paginationOptions
        );

        $this->form = $this->filterForm->createView();

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPagination()
    {
        return $this->pagination;
    }

    /**
     * @return mixed
     */
    public function getForm()
    {
        return $this->form;
    }

    /**
     * @return mixed
     */
    public function getItems()
    {
        return $this->pagination->getItems();
    }

    /**
     * @return mixed
     */
    public function getTotalItemCount()
    {
        return $this->pagination->getTotalItemCount();
    }

    /**
     * @return mixed
     */
    public function getCurrentPageNumber()
    {
        return $this->pagination->getCurrentPageNumber();
    }

    /**
     * @return mixed
     */
    public function getPageCount()
    {
        return intval(ceil($this->pagination->getTotalItemCount() / $this->pageSize));
    }

    /**
     * @param int $queryHint
     * @return self
     */
    public function setQueryHint(int $queryHint): self
    {
        $this->queryHint = $queryHint;

        return $this;
    }

    /**
     * @param int $pageSize
     * @return self
     */
    public function setPageSize(int $pageSize): self
    {
        $this->pageSize = $pageSize;

        return $this;
    }

    /**
     * Sets Paginator configuration.
     *
     * @param array $options
     *
     * @return FilterablePaginatedList
     */
    public function setPaginationOptions(array $options): self
    {
        $this->paginationOptions = $options;

        return $this;
    }
}
