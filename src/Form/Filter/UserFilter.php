<?php

/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace App\Form\Filter;

use App\Entity\Producer;
use App\Entity\User;
use Doctrine\ORM\EntityRepository;
use Lexik\Bundle\FormFilterBundle\Filter\Doctrine\ORMQuery;
use Lexik\Bundle\FormFilterBundle\Filter\Form\Type\ChoiceFilterType;
use Lexik\Bundle\FormFilterBundle\Filter\Form\Type\EntityFilterType;
use Lexik\Bundle\FormFilterBundle\Filter\Form\Type\TextFilterType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Security\Core\Security;

class UserFilter extends AbstractType
{
    /** @var Security */
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var User */
        $user = $this->security->getUser();

        $defaultPlaceholder = '-- Tout --';

        $builder->add('fullName', TextFilterType::class, [
            'apply_filter' => function (ORMQuery $filterQuery, $field, $values) {
                if (empty($values['value'])) {
                    return null;
                }
                // expressions that represent the condition
                $builder = $filterQuery->getExpressionBuilder();
                $expression = $builder->expr()->orX(
                    $builder->stringLike('u.lastName', $values['value']),
                    $builder->stringLike('u.firstName', $values['value'])
                );

                return $filterQuery->createCondition($expression);
            },
        ]);

        $builder->add('organization', TextFilterType::class);

        if (!$user->getProducer()) {
            $builder->add('producer', EntityFilterType::class, [
                'class' => Producer::class,
                'placeholder' => $defaultPlaceholder,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('po')->orderBy('po.name', 'ASC');
                },
            ]);
        }

        $builder->add('role', ChoiceFilterType::class, [
            'choices' => [
                'Utilisateur' => 'ROLE_USER',
                'Référent' => 'ROLE_MANAGER',
                'Administrateur' => 'ROLE_ADMIN',
                'Super administrateur' => 'ROLE_SUPER_ADMIN',
            ],
            'placeholder' => $defaultPlaceholder,
        ]);

        $builder->add('email', TextFilterType::class);
        $builder->add('lastLogin', TextFilterType::class, [
            'apply_filter' => false,
        ]);

        $builder->add('status', ChoiceFilterType::class, [
            'choices' => [
                'Bloqué' => 'locked',
                'En attente' => 'notEnabled',
                'Validé' => 'enabled',
            ],
            'placeholder' => $defaultPlaceholder,
            'apply_filter' => function (ORMQuery $filterQuery, $field, $values) {
                if (empty($values['value'])) {
                    return null;
                }
                $builder = $filterQuery->getExpressionBuilder();
                switch ($values['value']) {
                    case 'locked':
                        return $filterQuery->createCondition($builder->expr()->eq('u.locked', 'false'));
                    case 'notEnabled':
                        return $filterQuery->createCondition($builder->expr()->eq('u.enabled', 'false'));
                    case 'enabled':
                        return $filterQuery->createCondition($builder->expr()->eq('u.enabled', 'true'));
                    default:
                        return;
                }
            },
        ]);
    }
}
