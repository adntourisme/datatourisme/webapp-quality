<?php

/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace App\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

/**
 * Class InvitationType.
 */
class InvitationType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->remove('email');
        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            $event->getForm()
                ->add('email', EmailType::class, [
                    'data' => $event->getData()->getEmail(),
                    'label' => 'Email',
                    'mapped' => false,
                    'required' => false,
                    'disabled' => true,
                ])
                ->add('password', StrongPasswordType::class, ['user' => $event->getData()]);
        });
    }
    public function getParent(): string
    {
        return AccountType::class;
    }
}
