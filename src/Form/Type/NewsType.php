<?php

/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: Apache-2.0
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace App\Form\Type;

use App\Entity\News;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Mime\MimeTypes;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;

class NewsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('date', DateType::class, [
                'required' => true,
                'widget' => 'single_text',
                'label' => 'Date',
            ])
            ->add('title', TextType::class, [
                'attr' => [
                    'placeholder' => "Titre de l'actualité...",
                ],
                'required' => true,
                'label' => 'Titre',
            ])
            ->add('body', TextareaType::class, [
                'attr' => [
                    'placeholder' => "Contenu de l'actualité...",
                    'rows' => '10',
                ],
                'required' => true,
                'label' => 'Texte',
            ]);

        $builder->addEventListener(FormEvents::POST_SET_DATA, [$this, 'onPostSetData']);
    }

    /**
     * onPostSetData
     */
    public function onPostSetData(FormEvent $event)
    {
        $form = $event->getForm();
        if ($event->getData()->getFilename()) {
            $form->add('removeFile', CheckboxType::class, [
                'label' => 'Supprimer le fichier joint',
                'mapped' => false,
                'required' => false,
            ]);
        } else {
            $allowedExtensions = self::getExtensionsFromTypes(News::getAllowedMimeTypes());
            sort($allowedExtensions);
            $form->add('file', FileType::class, [
                'mapped' => false,
                'required' => false,
                'label' => 'Fichier joint',
                'help' => 'Le fichier joint est limité à 5Mo. <br><span class="text-sm">Extensions autorisées : ' . implode(', ', $allowedExtensions) . '</span>',
                'help_html' => true,
                'constraints' => [
                    new File([
                        'maxSize' => '5120k',
                        'mimeTypes' => News::getAllowedMimeTypes(),
//                        'mimeTypesMessage' => 'Please upload a valid PDF document',
                    ])
                ],
            ]);
        }
    }

    /**
     * configureOptions
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => News::class,
        ]);
    }

    /**
     * @param array $types
     *
     * @return array
     */
    public static function getExtensionsFromTypes(array $types): array
    {
        $mimeTypes = new MimeTypes();
        $data = [];
        foreach ($types as $type) {
            $data = array_merge($data, $mimeTypes->getExtensions($type));
        }

        return array_unique($data);
    }
}
