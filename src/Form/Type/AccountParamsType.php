<?php

/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace App\Form\Type;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AccountParamsType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     * @return void
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('periodicNotifications', ChoiceType::class, [
                'label' => 'Notifications périodiques',
                'choices' => [
                    "Mensuelle" => "M",
                    "Hebdomadaire" => "W",
                    "Quotidienne" => "D",
                ],
                'required' => false,
                'expanded' => true,
                'multiple' => true,
            ])
        ;
        foreach ($options['anomalyTypes'] as $anomalyType => $anomalyLabel) {
            $builder
                ->add('anomaly_' . $anomalyType, CheckboxType::class, [
                    'label' => $anomalyLabel,
                    'required' => false,
                    'mapped' => false,
                ])
                ->add('threshold_' . $anomalyType, IntegerType::class, [
                    'label' => false,
                    'attr' => [
                        'min' => 1,
                        'max' => 100,
                        'style' => 'height: 25px; margin-top: 8px; width: 100px'
                    ],
                    'required' => false,
                    'mapped' => false,
                ])
            ;
        }
        $builder->addEventListener(FormEvents::POST_SUBMIT, function (FormEvent $event) use ($options) {
            $form = $event->getForm();
            foreach ($options['anomalyTypes'] as $anomalyType => $anomalyLabel) {
                if ($form->get("anomaly_$anomalyType")->getData()) {
                    $threshold = $form->get("threshold_$anomalyType")->getData();
                    if (!$threshold) {
                        $form->get("threshold_$anomalyType")->addError(
                            new FormError("Vous devez sélectionner un seuil en % (entre 1 et 100).")
                        );
                    } elseif ($threshold < 1. || $threshold > 100.) {
                        $form->get("threshold_$anomalyType")->addError(
                            new FormError("La valeur doit être comprise entre 1 et 100 (%).")
                        );
                    }
                }
            }
        });
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
            'anomalyTypes' => [
                "links" => "Anomalie de lien web",
                "media" => "Anomalie de média",
                "text" => "Anomalie de contenu",
                "geoCoordinates" => "Anomalie géospatiale",
                "consistency" => "Anomalie de cohérence",
            ],
        ]);
    }
}
