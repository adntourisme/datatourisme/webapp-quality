<?php

/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace App\Form\Type;

use App\Entity\Producer;
use App\Entity\User;
use App\Security\Voter\UserVoter;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Role\RoleHierarchyInterface;

/**
 * Class UserType.
 */
class UserType extends AbstractType
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var RoleHierarchyInterface
     */
    protected $roleHierarchy;

    /**
     * @var AuthorizationCheckerInterface
     */
    protected $authorizationChecker;

    public function __construct(EntityManagerInterface $em, RoleHierarchyInterface $roleHierarchy, AuthorizationCheckerInterface $authorizationChecker)
    {
        $this->em = $em;
        $this->roleHierarchy = $roleHierarchy;
        $this->authorizationChecker = $authorizationChecker;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addEventListener(FormEvents::PRE_SET_DATA, [$this, 'onPreSetData']);
    }

    /**
     * Add the organization field for new user OR ROLE_USER user.
     *
     * @param FormEvent $event
     */
    public function onPreSetData(FormEvent $event)
    {
        /** @var User $account */
        $account = $event->getData();
        $form = $event->getForm();

        if ($this->authorizationChecker->isGranted(UserVoter::SET_ROLE, $account)) {
            // create builder for field
            $builder = $form->getConfig()->getFormFactory()->createNamedBuilder('role', RoleType::class, null, [
                'choice_label' => [
                    'ROLE_USER' => 'Utilisateur',
                    'ROLE_MANAGER' => 'Référent',
                    'ROLE_ADMIN' => 'Administrateur',
                    'ROLE_SUPER_ADMIN' => 'Super administrateur',
                ],
                'auto_initialize' => false // it's important!!!
            ]);
            $builder->addEventListener(FormEvents::POST_SUBMIT, [$this, 'onRolePostSubmit']);
            $form->add($builder->getForm());
        }

        $this->addProducerField($form, $account->getRole(), $account);
        $this->addHasBeenCreatedByField($form, $account->getRole(), $account->getProducer(), $account);
    }

    /**
     * Role post submit
     */
    public function onRolePostSubmit(FormEvent $event)
    {
        $role = $event->getForm()->getData();
        $form = $event->getForm()->getParent();
        $this->addProducerField($form, $role, $form->getData());
    }

    /**
     * Add additional form element or set some null values on main entity
     */
    private function addProducerField(FormInterface $form, string $role, User $account)
    {
        if (!in_array($role, ['ROLE_MANAGER', 'ROLE_USER'])) {
            $account->setProducer(null);
            return;
        }

        if ($this->authorizationChecker->isGranted(UserVoter::SET_PRODUCER, $account)) {
            // create builder for field
            $builder = $form->getConfig()->getFormFactory()->createNamedBuilder('producer', EntityType::class, null, [
                'class' => Producer::class,
                'label' => "SIT Producteur",
                'required' => !($role == 'ROLE_ADMIN'),
                'placeholder' =>  "",
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('p')
                        ->orderBy('p.name', 'ASC');
                },
                'help' => 'SIT sur la plateforme producteur',
                'auto_initialize' => false // it's important!!!
            ]);
            $builder->addEventListener(FormEvents::POST_SUBMIT, function (FormEvent $event) use ($role) {
                $this->onProducerPostSubmit($event, $role);
            });
            $form->add($builder->getForm());
        }
    }

    /**
     * Role post submit
     */
    public function onProducerPostSubmit(FormEvent $event, string $role)
    {
        $producer = $event->getForm()->getData();
        $form = $event->getForm()->getParent();
        $this->addHasBeenCreatedByField($form, $role, $producer, $form->getData());
    }

    /**
     * Add additional form element or set some null values on main entity
     */
    private function addHasBeenCreatedByField(FormInterface $form, string $role, ?Producer $producer, User $account)
    {
        if ($role != 'ROLE_USER') {
            $account->setHasBeenCreatedBy(null);
            return;
        }

        if ($this->authorizationChecker->isGranted(UserVoter::ADMINISTRATE, $account)) {
            $form->add('hasBeenCreatedBy', HasBeenCreatedByType::class, [
                'producer' => $producer,
                'label' => "Créateurs",
                'help' => "Liste des créateurs de données associés à l'utilisateur",
                'required' => false
            ]);
        }
    }

    public function getParent()
    {
        return AccountType::class;
    }
}
