<?php

/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace App\Controller;

use App\Elasticsearch\DatatourismeExport;
use App\Elasticsearch\DatatourismeIndex;
use App\Elasticsearch\StatisticsIndex;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/es", name="elasticsearch.")
*/
class ElasticsearchController extends AbstractController
{
    /**
     * @Route("", name="index", methods={"GET", "HEAD"})
     */
    public function index(): Response
    {
        // needed for ReactiveSearch base
        throw new NotFoundHttpException();
    }

    /**
     * @Route("/datatourisme/{path}", name="datatourisme", methods={"POST"}, requirements={"path"="_search|_msearch|_count"})
     * @Route("/datatourisme/{path}", name="_datatourisme", methods={"GET"}, requirements={"path"="_doc/.*"})
     */
    public function datatourisme(string $path, Request $request, DatatourismeIndex $datatourismeIndex): Response
    {
        $alter = !$request->query->has('all');
        return $datatourismeIndex->proxy($request, $path, $alter);
    }

    /**
     * @Route("/datatourisme/export/{query}/{format}", name="datatourisme.export", methods={"GET"}, requirements={"format"="csv|xlsx|ods"})
     */
    public function datatourismeExport(string $query, string $format = "csv", Request $request, DatatourismeExport $datatourismeExport): Response
    {
        return $datatourismeExport->download($query, $format);
    }

    /**
     * @Route("/statistics/{path}", name="statistics", methods={"POST"}, requirements={"path"="_search|_msearch|_count"})
     * @Route("/statistics/{path}", name="_statistics", methods={"GET"}, requirements={"path"="_doc/.*"})
     */
    public function statistics(string $path, Request $request, StatisticsIndex $statisticsIndex): Response
    {
        $alter = !$request->query->has('all');
        return $statisticsIndex->proxy($request, $path, $alter);
    }
}
