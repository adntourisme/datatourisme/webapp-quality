<?php

/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/explorer", name="explorer.")
*/
class ExplorerController extends AbstractController
{
    /**
     * @Route("", name="index", methods={"GET", "HEAD"})
     */
    public function index(): Response
    {
        return $this->render('explorer/index.html.twig', []);
    }
}
