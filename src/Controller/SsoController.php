<?php

/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace App\Controller;

use App\Entity\OrganizationType;
use App\Entity\Producer;
use App\Entity\User;
use App\Repository\ProducerRepository;
use App\Repository\UserRepository;
use App\Security\UserChecker;
use App\Service\SsoEndpoint;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Signature\Exception\ExpiredSignatureException;
use Symfony\Component\Security\Http\Authentication\UserAuthenticatorInterface;
use Symfony\Component\Security\Http\Authenticator\FormLoginAuthenticator;

/**
 * @Route("/sso", name="sso.")
 */
class SsoController extends AbstractController
{
    private EntityManagerInterface $em;
    private UserRepository $userRepository;
    private ProducerRepository $producerRepository;
    private SsoEndpoint $endpoint;
    private UserAuthenticatorInterface $userAuthenticator;
    private FormLoginAuthenticator $formLoginAuthenticator;
    private UserChecker $userChecker;

    /**
    * Constructor
    */
    public function __construct(
        EntityManagerInterface $em,
        SsoEndpoint $endpoint,
        UserRepository $userRepository,
        ProducerRepository $producerRepository,
        UserAuthenticatorInterface $userAuthenticator,
        FormLoginAuthenticator $formLoginAuthenticator,
        UserChecker $userChecker
    ) {
        $this->em = $em;
        $this->endpoint = $endpoint;
        $this->userRepository = $userRepository;
        $this->producerRepository = $producerRepository;
        $this->userAuthenticator = $userAuthenticator;
        $this->formLoginAuthenticator = $formLoginAuthenticator;
        $this->userChecker = $userChecker;
    }

    /**
     * @Route("/login", name="login", methods={"GET"})
     */
    public function login(Request $request): Response
    {
        $sso = $request->get('sso');
        $signature = $request->get('sig');

        if (!$sso || !$signature) {
            return $this->endpoint->redirect();
        }

        try {
            $payload = $this->endpoint->decode($sso, $signature);

            if (!empty($payload['error'])) {
                $this->addFlash('error', $payload['error']);

                return $this->redirectToRoute('security.login');
            }

            if (empty($payload['qa_access']) || empty($payload['organization'])) {
                // this account cannot used sso on this platform
                $this->addFlash('error', "Vous n'êtes pas autorisé à vous connecter avec ce compte.");

                return $this->redirectToRoute('security.login');
            }

            // create/update user
            $user = $this->retrieveUser($payload);

            // important ?
            // $request->getSession()->migrate();

            // then login
            try {
                $this->userChecker->checkPreAuth($user);
                $response = $this->userAuthenticator->authenticateUser($user, $this->formLoginAuthenticator, $request);
                $this->userChecker->checkPostAuth($user);
                $this->addFlash('success', "Vous êtes maintenant connecté en tant que " . $user->getFullName() . ".");

                return $response;
            } catch(AuthenticationException $e) {
                $this->addFlash('error', $e->getMessage());

                return $this->redirectToRoute('security.login');
            }
        } catch (ExpiredSignatureException $e) {
            // restart process
            return $this->redirectToRoute('sso.login');
        } catch (\Exception $e) {
            throw new BadRequestHttpException(null, $e);
        }
    }

    /**
     * Find or create the User based on the SSO payload
     */
    private function retrieveUser($payload): User
    {
        $user = $this->userRepository->findOneBy(['email' => $payload['email']]);
        if (!$user) {
            $user = new User();
            $user->setEmail($payload['email']);
            $user->setPlainPassword(md5(time()));
            $user->setRole('ROLE_USER'); // do not assign ROLE_MANAGER to avoid wide permission
            $user->setEnabled(true);
            $this->em->persist($user);
        }

        // update some fields
        $user->setExternal(true);
        $user->setFirstName($payload['first_name']);
        $user->setLastName($payload['last_name']);
        if (!empty($payload['organization'])) {
            $user->setOrganization($payload['organization']['name']);
            $user->setProducer($this->retrieveProducer($payload['organization']));
        } else {
            $user->setOrganization('NON RENSEIGNÉ');
        }

        // flush
        $this->em->flush();

        return $user;
    }

    /**
     * Find or create the User based on the SSO payload
     */
    private function retrieveProducer($payload): Producer
    {
        $producer = $this->producerRepository->find($payload['id']);
        if (!$producer) {
            $producer = new Producer();
            $producer->setId($payload['id']);
            $producer->setName($payload['name']);

            if (!empty($payload['type'])) {
                $id = $payload['type'] == 2 ? 1 : $payload['type']; // [1,2] Agences de développement touristique, Comité départemental de tourisme
                $type = $this->em->getRepository(OrganizationType::class)->find($id);
                $producer->setType($type);
            }

            $this->em->persist($producer);
        }

        return $producer;
    }
}
