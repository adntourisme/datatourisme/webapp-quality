<?php

/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace App\Controller\Administration;

use App\Entity\Producer;
use App\Entity\User;
use App\Form\Filter\UserFilter;
use App\Form\Type\UserType;
use App\Notification\InvitationNotification;
use App\Pagination\FilterablePaginatedListFactory;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Notifier\NotifierInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * @Route("/admin/user", name="user.")
 * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_MANAGER')")
 */
class UserController extends AbstractController
{
    /**
     * @Route("", name="index")
     */
    public function index(Request $request, FilterablePaginatedListFactory $factory, EntityManagerInterface $em): Response
    {
        $query = $em->getRepository(User::class)->getIndexQueryBuilder();

        $list = $factory->getList($query, UserFilter::class)
//            ->setPageSize(5)
            ->handleRequest($request);

        $producer = null;
        // if there is an organization filter, select it
        $filters = $list->getPagination()->getParams();
        if (!empty($filters['user_filter']['producer'])) {
            $producer = $em->getRepository(Producer::class)->find((int) $filters['user_filter']['producer']);
        }

        return $this->render('admin/user/index.html.twig', [
            'producer' => $producer,
            'list' => $list,
        ]);
    }

    /**
     * @Route("/add", name="add")
     * @IsGranted("user.create")
     *
     * @param Request $request
     * @param UserPasswordHasherInterface $userPasswordHasher
     * @param NotifierInterface $notifier
     * @return Response
     */
    public function add(
        Request $request,
        UserPasswordHasherInterface $userPasswordHasher,
        NotifierInterface $notifier,
        EntityManagerInterface $em
    ): Response {
        /** @var User $currentUser */
        $currentUser = $this->getUser();

        $user = new User();
        $user->setCreatedBy($currentUser);
        $user->setRole($this->isGranted('ROLE_ADMIN') ? 'ROLE_MANAGER' : 'ROLE_USER');
        $user->setProducer($currentUser->getProducer());
        $user->setPlainPassword(strval(rand(10000000, 9999999999999)));
        $user->setEnabled(false);

        $userForm = $this->createForm(UserType::class, $user);
        $userForm->handleRequest($request);
        if ($userForm->isSubmitted() && $userForm->isValid()) {
            // persist new user
            $em->persist($user);
            $em->flush();

            // generate token from password
            $bcrypt = $userPasswordHasher->hashPassword($user, $user->getPassword());

            // send invitation email with link
            $notification = new InvitationNotification($currentUser, $user, $bcrypt);
            $notifier->send($notification, $user->toRecipient());

            $this->addFlash('success', 'L\'utilisateur a bien été invité.');
            return new JsonResponse([
                'success' => true,
                'redirect' => $this->generateUrl('user.index'),
            ], 200);
        }

        return $this->render('admin/user/add.html.twig', [
            'form' => $userForm->createView(),
            'user' => $user,
        ]);
    }

    /**
     * @Route("/edit/{id}", name="edit")
     * @IsGranted("user.edit", subject="user")
     *
     * @param Request $request
     * @param User|null $user
     * @return Response
     */
    public function edit(Request $request, User $user, EntityManagerInterface $em): Response
    {
        $userForm = $this->createForm(UserType::class, $user);
        $userForm->handleRequest($request);
        if ($userForm->isSubmitted() && $userForm->isValid()) {
            $em->flush();
            $this->addFlash('success', 'L\'utilisateur a bien été mis à jour.');

            return $this->redirectToRoute('user.edit', ['id' => $user->getId()]);
        }

        return $this->render('admin/user/edit.html.twig', [
            'form' => $userForm->createView(),
            'user' => $user,
        ]);
    }

    /**
     * @Route("/edit/{id}/administrate", name="administrate")
     *
     * @param User|null $user
     *
     * @return Response
     */
    public function administrate(User $user): Response
    {
        return $this->render('admin/user/administrate.html.twig', [
            'user' => $user,
        ]);
    }

    /**
     * @Route("/edit/{id}/administrate/block", name="administrate.block")
     * @IsGranted("user.block", subject="user")
     *
     * @param Request $request
     * @param User    $user
     *
     * @return Response
     */
    public function block(Request $request, User $user, EntityManagerInterface $em): Response
    {
        if ($request->getMethod() === 'POST') {
            $user->setLocked(!$user->isLocked());
            $em->flush();
            // $this->get('webapp.mailer')->send('account.blocked', $account);
            $this->addFlash(($user->isLocked() ? 'warning' : 'success'), 'L\'utilisateur a été ' . ($user->isLocked() ? 'bloqué.' : 'débloqué.'));

            return new JsonResponse([
                'success' => true,
                'reload' => true,
            ], 200);
        }

        return $this->render('admin/user/administrate/block.html.twig', [
            'user' => $user,
        ]);
    }

    /**
     * @Route("/edit/{id}/administrate/delete", name="administrate.delete")
     * @IsGranted("user.delete", subject="user")
     */
    public function delete(Request $request, User $user, EntityManagerInterface $em): Response
    {
        if ($request->getMethod() === 'POST') {
            // $this->get('webapp.mailer')->send('account.deleted', $account);
            $em->remove($user);
            $em->flush();
            $this->addFlash('warning', 'L\'utilisateur a bien été supprimé.');

            return new JsonResponse([
                'success' => true,
                'redirect' => $this->generateUrl('user.index'),
            ], 200);
        }

        return $this->render('admin/user/administrate/delete.html.twig', [
            'user' => $user,
        ]);
    }

    /**
     * @Route("/edit/{id}/administrate/invite", name="administrate.invite")
     * @IsGranted("user.administrate", subject="user")
     *
     * @param User $user
     * @param UserPasswordHasherInterface $userPasswordHasher
     * @param NotifierInterface $notifier
     * @return Response
     */
    public function invite(
        User $user,
        UserPasswordHasherInterface $userPasswordHasher,
        NotifierInterface $notifier
    ): Response {

        if ($user->isEnabled()) {
            throw new BadRequestHttpException();
        }

        // generate token from password
        $bcrypt = $userPasswordHasher->hashPassword($user, $user->getPassword());

        // send invitation email with link
        $notification = new InvitationNotification($this->getUser(), $user, $bcrypt);
        $notifier->send($notification, $user->toRecipient());

        $this->addFlash('success', 'L\'utilisateur a bien été invité.');

        return $this->redirectToRoute('user.administrate', ['id' => $user->getId()]);
    }
}
