<?php

/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace App\Notification;

use App\Entity\News;
use Symfony\Component\Notifier\Message\EmailMessage;
use Symfony\Component\Notifier\Notification\EmailNotificationInterface;
use Symfony\Component\Notifier\Notification\Notification;
use Symfony\Component\Notifier\Recipient\EmailRecipientInterface;
use Symfony\Component\Notifier\Recipient\RecipientInterface;

class NewsNotification extends Notification implements EmailNotificationInterface
{
    private $news;

    public function __construct(News $news)
    {
        $this->news = $news;
        parent::__construct('Nouvelle actualité sur la plateforme qualité');
    }

    public function getChannels(RecipientInterface $recipient): array
    {
        $this->importance('DATAtourisme');
        return ['email'];
    }

    public function asEmailMessage(EmailRecipientInterface $recipient, string $transport = null): ?EmailMessage
    {
        $message = EmailMessage::fromNotification($this, $recipient, $transport);

        /** @var NotificationEmail */
        $email = $message->getMessage();
        $email
            ->htmlTemplate('email/news/news_creation.html.twig')
            ->context(['news' => $this->news])
        ;

        return $message;
    }
}
