<?php

/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace App\Notification;

use Symfony\Bridge\Twig\Mime\NotificationEmail;
use Symfony\Component\Notifier\Message\EmailMessage;
use Symfony\Component\Notifier\Notification\EmailNotificationInterface;
use Symfony\Component\Notifier\Notification\Notification;
use Symfony\Component\Notifier\Recipient\EmailRecipientInterface;
use Symfony\Component\Notifier\Recipient\RecipientInterface;

class AnomalyReportNotification extends Notification implements EmailNotificationInterface
{
    private array $report;
    private string $type;
    private array $anomalyTypes = [
        "LinksAnalyzer" => "Anomalie de lien web",
        "MediaAnalyzer" => "Anomalie de média",
        "TextAnalyzer" => "Anomalie de contenu",
        "GeoCoordinatesAnalyzer" => "Anomalie géospatiale",
        "ConsistencyAnalyzer" => "Anomalie de cohérence",
    ];
    private array $horizons = [
        "day" => "Jour",
        "week" => "Semaine",
        "month" => "Mois",
        "trimester" => "Trimestre",
        "semester" => "Semestre",
    ];

    public function __construct(array $report, string $type)
    {
        $this->report = $report;
        $this->type = $type;

        if ('threshold' == $this->type) {
            parent::__construct("Alerte sur l'évolution quotidienne des anomalies");
        } else {
            parent::__construct('Rapport ' . ['daily' => 'quotidien', 'weekly' => 'hebdomadaire', 'monthly' => 'mensuel'][$this->type] . ' sur les anomalies');
        }
    }

    public function getChannels(RecipientInterface $recipient): array
    {
        $this->importance("DATAtourisme");
        return ['email'];
    }

    public function asEmailMessage(EmailRecipientInterface $recipient, string $transport = null): ?EmailMessage
    {
        $message = EmailMessage::fromNotification($this, $recipient, $transport);

        /** @var NotificationEmail */
        $email = $message->getMessage();
        if ($this->type == 'threshold') {
            $email
                ->htmlTemplate('email/report/anomaly_threshold_report.html.twig')
                ->context([
                    'report' => $this->report,
                    'types' => $this->anomalyTypes,
                ])
            ;
        } else {
            $email
                ->htmlTemplate('email/report/anomaly_periodic_report.html.twig')
                ->context([
                    'report' => $this->report,
                    'types' => $this->anomalyTypes,
                    'horizons' => $this->horizons,
                    'period' => $this->type,
                    'horizon' => ['daily' => 'day', 'weekly' => 'week', 'monthly' => 'month'][$this->type],
                ])
            ;
        }

        return $message;
    }
}
