<?php

/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace App\EventListener;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\EventSubscriber\EventSubscriberInterface;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;
use Symfony\Component\PasswordHasher\Hasher\PasswordHasherFactoryInterface;

class PasswordEncoderSubscriber implements EventSubscriberInterface
{
    /**
     * @var PasswordHasherFactoryInterface
     */
    protected $passwordHasherFactory;

    /**
     * PasswordEncoderSubscriber constructor.
     *
     * @param PasswordHasherFactoryInterface $encoderFactory
     */
    public function __construct(PasswordHasherFactoryInterface $passwordHasherFactory)
    {
        $this->passwordHasherFactory = $passwordHasherFactory;
    }

    /**
     * @return array
     */
    public function getSubscribedEvents()
    {
        return [
            Events::prePersist,
            Events::preUpdate,
        ];
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function prePersist(LifecycleEventArgs $args)
    {
        $this->preUpdate($args);
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function preUpdate(LifecycleEventArgs $args)
    {
        /** @var User */
        $user = $args->getEntity();
        if (!($user instanceof User)) {
            return;
        }

        $plainPassword = $user->getPlainPassword();
        if (!empty($plainPassword)) {
            $hasher = $this->passwordHasherFactory->getPasswordHasher($user);
            $user->setPassword($hasher->hash($plainPassword));
            $user->eraseCredentials();
        }
    }
}
