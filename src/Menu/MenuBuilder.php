<?php

/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace App\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class MenuBuilder
{
    /** @var FactoryInterface */
    private $factory;

    /** @var AuthorizationCheckerInterface */
    private $authorizationChecker;

    /**
     * @param FactoryInterface $factory
     *
     * Add any other dependency you need
     */
    public function __construct(FactoryInterface $factory, AuthorizationCheckerInterface $authorizationChecker)
    {
        $this->factory = $factory;
        $this->authorizationChecker = $authorizationChecker;
    }

    public function createMainMenu(array $options)
    {
        $menu = $this->factory->createItem('root');

        // Dashboard
        $menu->addChild('Tableau de bord', [
            'route' => 'dashboard.index',
            'extras' => [
                'icon' => 'fa fa-dashboard',
            ],
        ]);

        // Explorer
        $menu->addChild('Explorer', [
            'route' => 'explorer.index',
            'extras' => [
                'icon' => 'fa fa-search',
            ],
        ]);

        // Monitor
        $menu->addChild('Anomalies', [
            'route' => 'monitor.index',
            'extras' => [
                'icon' => 'fa fa-exclamation-circle',
            ],
        ]);

        // News
        $menu->addChild('Actualités', [
            'route' => 'news.index',
            'extras' => [
                'uri_prefix' => '/news',
                'icon' => 'fa fa-bullhorn',
            ],
        ]);

        // Administration
        if ($this->authorizationChecker->isGranted('ROLE_ADMIN') || $this->authorizationChecker->isGranted('ROLE_MANAGER')) {
            $admin = $menu->addChild('Administration', [
                'uri' => '#',
                'extras' => [
                    'uri_prefix' => '/admin',
                    'icon' => 'fa fa-cogs',
                ],
            ]);

            $admin->addChild('Utilisateurs', ['route' => 'user.index']);
        }

        return $menu;
    }
}
