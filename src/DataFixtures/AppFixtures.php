<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AppFixtures extends Fixture
{
    /** @var UserPasswordHasherInterface  */
    protected $passwordEncoder;

    public function __construct(UserPasswordHasherInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager): void
    {
        $user = new User();
        $user->setEmail('admin@localhost.dev');
        $user->setRole('ROLE_SUPER_ADMIN');
        $hashedPassword = $this->passwordEncoder->hashPassword($user, 'admin');
        $user->setPassword($hashedPassword);
        $user->setFirstName('Admin');
        $user->setLastName('Admin');
        $user->setOrganization('DATAtourisme');
        $user->setEnabled(true);
        $user->setLocked(false);
        $manager->persist($user);
        $manager->flush();
    }
}
