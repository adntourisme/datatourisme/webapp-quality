<?php

/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace App\Security\Voter;

use App\Entity\User;
use App\Security\Voter\AbstractVoter;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

class UserVoter extends AbstractVoter
{
    const CREATE = 'user.create';
    const EDIT = 'user.edit';
    const DELETE = 'user.delete';
    const ADMINISTRATE = 'user.administrate';
    const IMPERSONATE = 'user.impersonate';
    const BLOCK = 'user.block';
    const SET_ROLE = 'user.set_role';
    const SET_PRODUCER = 'user.set_producer';
    const MODIFY_PASSWORD = 'user.modify_password';

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        /** @var User */
        $user = $token->getUser();

        if (!$user instanceof User) {
            return false;
        }

        $account = $subject ? $subject : $user;

        switch ($attribute) {
            case self::CREATE:
                return $this->canCreate($user);
            case self::EDIT:
                return $this->canEdit($account, $user);
            case self::DELETE:
                return $this->canDelete($account, $user);
            case self::ADMINISTRATE:
                return $this->canAdministrate($account, $user);
            case self::BLOCK:
                return $this->canBlock($account, $user);
            case self::IMPERSONATE:
                return $this->canImpersonate($account, $user);
            case self::SET_ROLE:
                return $this->canSetRole($account, $user);
            case self::SET_PRODUCER:
                return $this->canSetProducer($account, $user);
            case self::MODIFY_PASSWORD:
                return $this->canModifyPassword($account, $user);
        }

        throw new \LogicException('This code should not be reached!');
    }

    /**
     * @return bool
     */
    private function canAdministrate(User $account, User $user): bool
    {
        // cannot administrate himself
        if ($account->getId() == $user->getId()) {
            return false;
        }

        // only superadmin can administrate admin
        if ($this->hasRole('ROLE_ADMIN', $account)) {
            return $this->hasRole('ROLE_SUPER_ADMIN', $user);
        }

        // producer can administrate user from the same SIT
        if ($this->hasRole('ROLE_MANAGER', $user) && $user->getProducer() && $user->getProducer() == $account->getProducer()) {
            return true;
        }

        // admin can administrate
        return $this->hasRole('ROLE_ADMIN', $user);
    }

    /**
     * @return bool
     */
    private function canCreate(User $user): bool
    {
        // return $this->hasRole('ROLE_MANAGER', $user);
        return $this->hasRole('ROLE_ADMIN', $user);
    }

    /**
     * @return bool
     */
    private function canEdit(User $account, User $user): bool
    {
        if ($this->canAdministrate($account, $user)) {
            return true;
        }

        return $account->getId() == $user->getId();
    }

    /**
     * @return bool
     */
    private function canDelete(User $account, User $user): bool
    {
        return $this->canAdministrate($account, $user);
    }

    /**
     * @return bool
     */
    private function canBlock(User $account, User $user): bool
    {
        return $this->canAdministrate($account, $user);
    }

    /**
     * @return bool
     */
    private function canImpersonate(User $account, User $user): bool
    {
        // nobody can impersonnate during impersonnation
        if ($this->isImpersonator()) {
            return false;
        }

        // disabled account cannot be impersonnated
        if (!$account->isEnabled()) {
            return false;
        }

        return $this->canAdministrate($account, $user);
    }

    /**
     * @return bool
     */
    private function canModifyPassword(User $account, User $user): bool
    {
        if ($this->isImpersonator()) {
            return false;
        }

        return $account->getId() === $user->getId();
    }

    /**
     * @return bool
     */
    private function canSetRole(User $account, User $user): bool
    {
        // only admin can set role
        if (!$this->hasRole('ROLE_ADMIN', $user)) {
            return false;
        }

        // not himself
        return $account->getId() != $user->getId();
    }

    /**
     * @return bool
     */
    private function canSetProducer(User $account, User $user): bool
    {
        // only admin can set producer
        if (!$this->hasRole('ROLE_ADMIN', $user)) {
            return false;
        }

        // not himself
        return $account->getId() != $user->getId();
    }
}
