<?php

/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: Apache-2.0
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace App\Security\Voter;

use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

/**
 * Class NewsVoter
 */
class NewsVoter extends AbstractVoter
{
    const EDIT = 'news.edit';
    const ADD = 'news.add';
    const REMOVE = 'news.remove';

    /**
     * {@inheritdoc}
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();

        if (!$user instanceof User) {
            return false;
        }

        switch ($attribute) {
            case self::EDIT:
                return $this->canEdit($user);
            case self::ADD:
                return $this->canAdd($user);
            case self::REMOVE:
                return $this->canDelete($user);
        }

        throw new \LogicException('This code should not be reached.');
    }

    /**
     * @param User $user
     *
     * @return bool
     */
    private function canEdit(User $user): bool
    {
        if ($this->hasRole('ROLE_ADMIN', $user)) {
            return true;
        }

        return false;
    }

    /**
     * @param User $user
     *
     * @return bool
     */
    private function canAdd(User $user): bool
    {
        return $this->canEdit($user);
    }

    /**
     * @param User $user
     *
     * @return bool
     */
    private function canDelete(User $user): bool
    {
        return $this->canEdit($user);
    }

    /**
     * @param User $user
     *
     * @return bool
     */
    public function canReorder(User $user): bool
    {
        return $this->canEdit($user);
    }
}
