<?php

/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

namespace App\Security\Voter;

use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Role\RoleHierarchyInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;

abstract class AbstractVoter extends Voter
{
    /**
     * @var RoleHierarchyInterface
     */
    private $roleHierarchy;

    /**
     * @var Security
     */
    private $security;

    /**
     * @param RoleHierarchyInterface $roleHierarchy
     */
    public function __construct(RoleHierarchyInterface $roleHierarchy, Security $security)
    {
        $this->roleHierarchy = $roleHierarchy;
        $this->security = $security;
    }

    /**
     * {@inheritdoc}
     */
    protected function supports($attribute, $subject): bool
    {
        $reflection = new \ReflectionClass($this);
        $grantings = array_diff($reflection->getConstants(), $reflection->getParentClass()->getConstants());
        if (!in_array($attribute, $grantings, true)) {
            return false;
        }

        return true;
    }

    /**
     * hasRole.
     *
     * @param string        $role
     * @param UserInterface $user
     *
     * @return bool
     */
    public function hasRole($role, UserInterface $user)
    {
        $reachableRoles = $this->roleHierarchy->getReachableRoleNames($user->getRoles());

        foreach ($reachableRoles as $reachableRole) {
            if ($reachableRole === $role) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param $token
     *
     * @return bool
     */
    public function isImpersonator()
    {
        return $this->security->isGranted('IS_IMPERSONATOR');
    }
}
