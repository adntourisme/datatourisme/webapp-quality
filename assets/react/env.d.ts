/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

/// <reference types="react-scripts" />
declare module "elasticsearch-browser" {
  import elasticsearch from "elasticsearch";
  import "elasticsearch-browser";
  export * from "elasticsearch";
}
