/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

import { DataSearch, ReactiveBase } from "@appbaseio/reactivesearch";
import AggregationsSidebar from "./components/AggregationsSidebar";
import CustomResultDataGrid from "./components/CustomResultDataGrid";
import PerfectScrollbar from "react-perfect-scrollbar";
import theme from "./theme.js";
import "./styles.scss";
import SearchToolbar from "./components/SearchToolbar";
import useQueryString from "../hooks/useQueryString";
import CustomResultMap from "./components/CustomResultMap";
import ElasticsearchClient from "../utils/es-client";
import React, { useState } from "react";
import ItemDrawer from "./components/drawer/ItemDrawer";

export type ExplorerContextParams = {
  client: ElasticsearchClient;
  currentQuery: any;
};

export const ExplorerContext = React.createContext<ExplorerContextParams>(null);

const Explorer = ({ url }: { url: string }) => {
  const client = new ElasticsearchClient(url);
  const [viewMode, setViewMode] = useQueryString<"map" | "list">("view", "list");
  const [selectedItem, setSelectedItem] = useQueryString<string | undefined>("selected", undefined);
  const [currentQuery, setCurrentQuery] = useState<string | undefined>(null);

  const react = {
    and: [
      "search",
      "type",
      "type2",
      "type3",
      "anomalyAnalyzer",
      "anomalyMessage",
      "mediaCount",
      "hasBeenCreatedBy",
      "isPartOfRegion",
      "isPartOfDepartment",
    ],
  };

  const onQueryChange = (prevQuery: any, nextQuery: any) => {
    setCurrentQuery(nextQuery);
  };

  return (
    <ExplorerContext.Provider value={{ client, currentQuery }}>
      <ItemDrawer uri={selectedItem} onClose={() => setSelectedItem(undefined)} />
      <ReactiveBase app="datatourisme" className="h-full" url={url} theme={theme}>
        <div className="flex h-full">
          <PerfectScrollbar className="w-80 p-3" options={{ wheelPropagation: false }}>
            <AggregationsSidebar />
          </PerfectScrollbar>
          <div className="flex-1 flex flex-col h-full">
            <SearchToolbar className="my-3" viewMode={viewMode} setViewMode={setViewMode} />
            {viewMode == "list" && (
              <CustomResultDataGrid
                componentId="datagrid"
                onLabelClick={(hit) => setSelectedItem(hit._id)}
                onQueryChange={onQueryChange}
                react={react}
              />
            )}
            {viewMode == "map" && (
              <CustomResultMap
                componentId="map"
                onFeatureClick={(hit) => setSelectedItem(hit._id)}
                onQueryChange={onQueryChange}
                selectedFeatureId={selectedItem}
                react={react}
              />
            )}
          </div>
        </div>
      </ReactiveBase>
    </ExplorerContext.Provider>
  );
};

export default Explorer;
