/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

import { useEffect, useRef, useState } from "react";
import { FormatterProps } from "react-data-grid";
import { DefaultValueFormatter } from "../../../components/ResultDataGrid";
import extract from "../../../utils/extract";
import Popper from "@mui/base/PopperUnstyled";

const MediaStatsFormatter = (props: FormatterProps<any>) => {
  const [anchorEl, setAnchorEl] = useState(null);

  const open = Boolean(anchorEl);

  let image = null;
  let images = extract(props.row, "hasRepresentation");
  if (images) {
    image =  Array.isArray(images) ? images[0] : images;
    image = extract(image, "hasRelatedResource.locator");
  }

  const onMouseOver = (e: HTMLElement) => {
    setAnchorEl(e.closest("[role=gridcell]"));
  }

  return (
    <>
      <div
        onMouseOver={(e) => onMouseOver(e.currentTarget)}
        onMouseOut={() => setAnchorEl(null)}
      >
        <DefaultValueFormatter {...props} />
      </div>
      {image && (
        <Popper open={open} anchorEl={anchorEl} placement="right" style={{zIndex: 999}}>
          <div className="bg-white shadow-lg border border-gray-300">
            <img className="object-cover h-48 w-96" src={image} alt={image}></img>
          </div>
        </Popper>
      )}
    </>
  );
};

export default MediaStatsFormatter;
