/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

import JsonViewer from "react-json-view";

const RawPanel = ({ data }: { data: { [key: string]: any } }) => {
  return (
    <div className="p-3">
      <div className="text-sm">
        <JsonViewer src={data} enableClipboard={false} displayDataTypes={false} collapsed={2} name="poi" />
      </div>
    </div>
  );
};

export default RawPanel;
