/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */
import { fromLonLat } from 'ol/proj'
import Feature from 'ol/Feature';
import Point from 'ol/geom/Point';

import ResultMap, { ResultMapProps } from "../../components/ResultMap";
import { types, typesByLabel } from '../../variables';

type CustomResultMapProps = {
  onFeatureClick: (hit: any) => void
} & Omit<ResultMapProps, "createFeature" | "onFeatureClick" | "dataField">;

const CustomResultMap = (props: CustomResultMapProps) => {
  const { onFeatureClick } = props;

  const createFeature = (hit: any): Feature<Point> => {
    const lat = hit.isLocatedAt.geoPoint.lat;
    const lon = hit.isLocatedAt.geoPoint.lon;
    const type = hit.analyze.type1[0];
    const color = typesByLabel[type]?.color;

    return new Feature({
      id: hit._id,
      geometry: new Point(fromLonLat([lon, lat])),
      color,
      hit
    })
  }

  return (
    <ResultMap
      dataField="isLocatedAt.geoPoint"
      includeFields={["isLocatedAt.geoPoint", "label", "analyze.type1"]}
      URLParams={true}
      size={5000}
      createFeature={createFeature}
      getTooltipContent={(f: Feature<Point>) => f.get('hit')['label']['@fr']}
      {...props}
      onFeatureClick={(f: Feature<Point>) => onFeatureClick(f.get("hit"))}
    >
      <div className='absolute z-10 w-48 bottom-3 left-3 bg-white bg-opacity-80 px-2 py-2 text-sm flex flex-col shadow-lg'>
        {Object.entries(types).map(([key, type]) => (
          <div key={key} className='mb-2 last:mb-0 flex items-center'>
            <div className='w-3 h-3 rounded-full mr-2' style={{ backgroundColor: type.color }}></div>
            <div>{type.label}</div>
          </div>
        ))}
      </div>
      </ResultMap>
  );
};

export default CustomResultMap;
