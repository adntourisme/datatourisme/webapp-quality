/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

export default {
  typography: {
    // fontFamily: '"Source Sans Pro","Helvetica Neue",Helvetica,Arial,sans-serif',
    fontFamily: 'ui-sans-serif, system-ui, -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji"',
    fontSize: "14px",
  },
  colors: {
    textColor: "#424242",
    primaryTextColor: "#fff",
    primaryColor: "#116390",
    titleColor: "#424242",
    alertColor: "#d9534f",
  },
};
