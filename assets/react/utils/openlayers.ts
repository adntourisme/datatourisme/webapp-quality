import { OSM, Source, WMTS } from "ol/source";
import {get as getProjection} from 'ol/proj';
import {getTopLeft, getWidth} from 'ol/extent';
import WMTSTileGrid from "ol/tilegrid/WMTS";
import TileSource from "ol/source/Tile";
import TileLayer from "ol/layer/Tile";

/**
 * Return the IGN source
 * @see https://geoservices.ign.fr/services-web-decouverte
 *
 * @returns
 */
const getIGNSource = (): WMTS => {
  const projection = getProjection('EPSG:3857');
  const projectionExtent = projection.getExtent();
  const size = getWidth(projectionExtent) / 256;
  const resolutions = new Array(19);
  const matrixIds = new Array(19);
  for (let z = 0; z < 19; ++z) {
    // generate resolutions and matrixIds arrays for this WMTS
    resolutions[z] = size / Math.pow(2, z);
    matrixIds[z] = z;
  }

  return new WMTS({
    attributions:
      'Tiles © <a href="https://mrdata.usgs.gov/geology/state/"' +
      ' target="_blank">USGS</a>',
    url: 'https://wxs.ign.fr/decouverte/geoportail/wmts',
    layer: 'GEOGRAPHICALGRIDSYSTEMS.PLANIGNV2',
    matrixSet: 'PM',
    format: 'image/png',
    projection: projection,
    tileGrid: new WMTSTileGrid({
      origin: getTopLeft(projectionExtent),
      resolutions: resolutions,
      matrixIds: matrixIds,
    }),
    style: 'normal',
    wrapX: true,
  });
}

/**
 * Return the default base source for the maps
 *
 * @returns
 */
 export const getDefaultBaseSource = (): TileSource => {
  return new OSM();
  // return getIGNSource();
}

/**
 * Return the default base source for the maps
 *
 * @returns
 */
 export const getDefaultBaseLayer = (): TileLayer<TileSource> => {
  return new TileLayer({ source: getDefaultBaseSource() });
}
