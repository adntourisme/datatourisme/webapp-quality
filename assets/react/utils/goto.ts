/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */
import qs from "query-string";

export const gotoExplorer = (query: { [key: string]: any }) => {
  const params = Object.fromEntries(Object.entries(query).map((e) => {
    if (Array.isArray(e[1])) {
      e[1] = `[${e[1].map(v => `"${e[1]}"`)}]`
    } else {
      e[1] = `"${e[1]}"`;
    }

    return e;
  }));

  const url = window.location.protocol + "//" + window.location.host + '/explorer?' + qs.stringify(params);
  window.location.href = url
};
