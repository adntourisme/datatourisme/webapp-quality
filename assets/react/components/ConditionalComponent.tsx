/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

import { StateProvider } from "@appbaseio/reactivesearch"
import { PropsWithChildren, ReactNode } from "react"

type ConditionalComponentProps = {
  deps: string[],
  children: (value: any) => ReactNode,
}

const ConditionalComponent = (props: ConditionalComponentProps) => {
  const { deps } = props;
  return <StateProvider
    includeKeys={["value"]}
    render={({ searchState }: any) => {
      for (const dep of deps) {
        const value = searchState[dep]?.value;
        if (value && (!Array.isArray(value) || value.length > 0)) {
          return props.children(searchState);
        }
      }

      return <></>;
    }}
  />
}

export default ConditionalComponent
