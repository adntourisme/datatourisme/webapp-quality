/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

import { MultiList as BaseMultiListProps } from "@appbaseio/reactivesearch/lib/components/list/MultiList";
import { MultiList as BaseMultiList, ReactiveComponent } from "@appbaseio/reactivesearch";
import useDebounce from "../hooks/useDebounce";
import { useCallback, useRef, useState } from "react";
import PerfectScrollbar from "perfect-scrollbar";
import Box from "./Box";

export type MultiListProps = {
  labels?: { [key: string]: string };
} & Omit<BaseMultiListProps, "showCheckbox">;

const MultiList = (props: Omit<MultiListProps, "showCheckbox">) => {
  const { title, showSearch, react, dataField, labels } = props;
  const [_searchValue, setSearchValue] = useState<string>("");
  const searchValue = useDebounce(_searchValue, 300);

  const boxRef = useRef(null);
  const perfectScrollRef = useRef(null);

  const transformData = (data) => {
    // this hack is used to instanciate perfectscroll bar
    const ul = boxRef.current.querySelector("ul");
    if (ul && !perfectScrollRef.current) {
      perfectScrollRef.current = new PerfectScrollbar(ul, {
        wheelPropagation: false,
      });
    }

    return data;
  };

  const customQuery = useCallback(() => {
    if (searchValue) {
      return {
        query: {
          wildcard: {
            [dataField]: {
              value: searchValue ? `*${searchValue}*` : "*",
              case_insensitive: true,
            },
          },
        },
      };
    }

    return null;
  }, [searchValue]);

  const renderItem = (key: string, doc_count: number, isChecked: boolean) => {
    const label = labels && labels[key] ? labels[key] : key;
    return (
      <span>
        <span className={isChecked ? 'font-semibold' : ''}>{label}</span>
        <span>{doc_count}</span>
      </span>
    );
  };

  // to handle active state, we tried to use controlled component feature, but it does not work :(
  // @see https://docs.appbase.io/docs/reactivesearch/v3/advanced/usage/

  return (
    <Box title={title} active={false} ref={boxRef}>
      {showSearch && (
        <>
          <div className="form-group m-0 has-feedback">
            <input
              type="text"
              className="form-control input-sm p-1 border-0 pl-8"
              placeholder="Rechercher..."
              onChange={(e) => setSearchValue(e.currentTarget.value)}
              value={_searchValue}
            />
            <span className="fa fa-search form-control-feedback text-gray-300 right-auto left-0" />
          </div>

          <ReactiveComponent componentId={`${props.componentId}___internal2`} customQuery={customQuery} />
        </>
      )}

      <BaseMultiList
        className="agg-multilist"
        showCheckbox={true}
        URLParams={true}
        size={10}
        // loader={<div className="text-center p-2 text-lg text-gray-400">Chargement...</div>}
        transformData={transformData}
        {...props}
        react={
          showSearch
            ? {
                ...react,
                and: [`${props.componentId}___internal2`, ...(react?.and as string[])],
              }
            : react
        }
        showSearch={false}
        title={null}
        renderItem={renderItem}
      />
    </Box>
  );
};

export default MultiList;
