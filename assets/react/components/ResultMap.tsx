/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

// @TODO
// use https://www.elastic.co/guide/en/elasticsearch/reference/current/search-aggregations-bucket-geohashgrid-aggregation.html
// see https://github.com/cyrilcherian/million-points-on-map#elastic-index-details
// to make server sive clustering

// @see https://openlayers.org/en/latest/examples/webgl-points-layer.html
// @see https://openlayers.org/workshop/en/webgl/points.html

import { ReactiveComponentProps } from "@appbaseio/reactivesearch/lib/components/basic/ReactiveComponent";
import { ReactiveComponent } from "@appbaseio/reactivesearch";
import { PropsWithChildren, useEffect, useRef, useState } from "react";

import { MapBrowserEvent, Overlay } from "ol";
import Map from "ol/Map";
import { getTopLeft, getBottomRight } from "ol/extent";
import { fromLonLat, toLonLat } from "ol/proj";
import View from "ol/View";
import TileLayer from "ol/layer/Tile";
import VectorSource from "ol/source/Vector";
import OSM from "ol/source/OSM";
import MapEvent from "ol/MapEvent";
import WebGLPointsLayer from "ol/layer/WebGLPoints";
import Feature from "ol/Feature";
import Point from "ol/geom/Point";
import { defaults as defaultControls, FullScreen } from "ol/control";
import Select, { SelectEvent } from "ol/interaction/Select";
import { click } from "ol/events/condition";

import { ReactiveComponentRenderProps } from "../interfaces";
import { BiInfoCircle } from "react-icons/bi";
import hexRgb from "hex-rgb";
import { getDefaultBaseLayer, getDefaultBaseSource } from "../utils/openlayers";

export type ResultMapProps = PropsWithChildren<{
  dataField: string;
  includeFields?: string[];
  createFeature: (doc: any) => Feature<Point>;
  onFeatureClick?: (feature: Feature<Point> | null) => void;
  getTooltipContent?: (feature: Feature<Point> | null) => string;
  selectedFeatureId?: string | number | null;
}> &
  Omit<ReactiveComponentProps, "children">;

// const style = {
//   "symbol": {
//     "symbolType": "image",
//     "src": "data/icon.png",
//     "size": [
//       18,
//       28
//     ],
//     "color": "lightyellow",
//     "rotateWithView": false,
//     "offset": [
//       0,
//       9
//     ]
//   }
// }

const getQueryForExtent = (field: string, extent: number[]): object => {
  const topLeft = toLonLat(getTopLeft(extent));
  const bottomRight = toLonLat(getBottomRight(extent));

  return {
    geo_bounding_box: {
      [field]: {
        top_left: {
          lon: topLeft[0],
          lat: topLeft[1],
        },
        bottom_right: {
          lon: bottomRight[0],
          lat: bottomRight[1],
        },
      },
    },
  };
};

const parseExtent = (value: string): number[] => {
  return value.split(",").map(parseFloat);
};

/**
 * ResultMapComponent
 */
const ResultMapComponent = (props: ReactiveComponentRenderProps & ResultMapProps) => {
  const {
    dataField,
    data,
    createFeature,
    onFeatureClick,
    selectedFeatureId,
    getTooltipContent,
    loading,
    setQuery,
    value,
    size,
    children,
  } = props;
  const mapElement = useRef<HTMLDivElement>(null);
  const tooltipElement = useRef<HTMLDivElement>(null);
  const center = [1.71, 46.71];
  const zoom = 6;

  const map = useRef<Map>(null);
  const layer = useRef<WebGLPointsLayer<any>>();

  const initialExtent = value ? parseExtent(value) : undefined;
  const [extent, setExtent] = useState<number[] | undefined>(initialExtent);

  // some refs
  const select = useRef<Select>(new Select({ condition: click }));
  const timeout = useRef<any>(null);

  // see https://openlayers.org/en/latest/examples/webgl-points-layer.html
  const style = useRef({
    variables: {
      selectedFeatureId: "" as any,
      hoveredFeatureId: "" as any,
    },
    symbol: {
      symbolType: "circle",
      size: ["interpolate", ["linear"], ["zoom"], 1, 1, 15, 15],
      // https://github.com/openlayers/openlayers/issues/10381
      // color: ["case", ["==", ["get", "id"], ["var", "selectedFeatureId"]], "#c02846", ['get', 'color']],
      color: [
        "case",
        ["==", ["get", "id"], ["var", "selectedFeatureId"]],
        "#c02846",
        ["color", ["get", "red"], ["get", "green"], ["get", "blue"]],
      ],
      // color: ["case", ["==", ["get", "id"], ["var", "selectedFeatureId"]], "#c02846", ["case", ["==", ["get", "id"], ["var", "hoveredFeatureId"]], "#62bbec", "#116390"]],
      offset: [0, 0],
      opacity: 0.95,
    },
  });

  useEffect(() => {
    style.current.variables.selectedFeatureId = (selectedFeatureId ? selectedFeatureId : "") as any;
    if (!selectedFeatureId) {
      select.current.getFeatures().clear();
    } else {
      // todo, programmaticaly select feature
      // select.current.getFeatures().push(featureToSelect)
    }
  }, [selectedFeatureId]);

  // initialize map on first render - logic formerly put into componentDidMount
  useEffect(() => {
    // create map
    const _map = new Map({
      target: mapElement.current!,
      layers: [getDefaultBaseLayer()],
      view: new View({
        projection: "EPSG:3857",
        center: fromLonLat(center),
        zoom,
      }),
      controls: defaultControls({
        zoom: true,
      }).extend([new FullScreen()]),
    });

    if (initialExtent) {
      _map.getView().fit(initialExtent);
    }

    _map.on("moveend", (evt: MapEvent) => {
      timeout.current = setTimeout(() => {
        setExtent(evt.map.getView().calculateExtent());
      }, 500);
    });

    _map.on("movestart", (evt: MapEvent) => {
      if (timeout.current) {
        clearTimeout(timeout.current);
        timeout.current = null;
      }
    });

    select.current.on("select", (e: SelectEvent) => {
      tooltipElement.current!.style.display = "none";
      onFeatureClick && onFeatureClick(e.selected.length ? e.selected[0] : null);
    });
    _map.addInteraction(select.current);

    var overlay = new Overlay({
      element: tooltipElement.current!,
      offset: [10, 0],
      positioning: "bottom-left",
    });
    _map.addOverlay(overlay);

    _map.on("pointermove", (evt: MapBrowserEvent<PointerEvent>) => {
      if (!getTooltipContent) {
        return;
      }
      var pixel = evt.pixel;
      var feature = map.current.forEachFeatureAtPixel(pixel, (feature) => feature);
      tooltipElement.current!.style.display = feature ? "" : "none";
      if (feature) {
        map.current.getViewport().style.cursor = "pointer";
        style.current.variables.hoveredFeatureId = feature.get("id");
        overlay.setPosition(evt.coordinate);
        tooltipElement.current!.innerHTML = getTooltipContent(feature as Feature<Point>);
      } else {
        map.current.getViewport().style.cursor = "";
        style.current.variables.hoveredFeatureId = "";
      }

      // layer.current?.changed(); DOES NOT WORK
    });

    // save map and vector layer references to state
    map.current = _map;
  }, []);

  // update markers
  useEffect(() => {
    if (map) {
      const features = data.map((hit: any) => {
        const feature = createFeature(hit);
        const properties = feature.getProperties();
        if (properties.color) {
          // @see https://github.com/openlayers/openlayers/issues/10381
          feature.setProperties({ ...properties, ...hexRgb(properties.color) });
        }

        return feature;
      });

      const _layer = new WebGLPointsLayer({
        source: new VectorSource({ features }),
        style: style.current,
        disableHitDetection: false,
      });

      if (layer.current) {
        map.current.removeLayer(layer.current);
      }

      map.current.addLayer(_layer);
      layer.current = _layer;
    }
  }, [JSON.stringify(data)]);

  // on bounds update (debounced)
  useEffect(() => {
    if (extent) {
      setQuery({
        query: getQueryForExtent(dataField, extent),
        value: extent.join(","),
      });
    }
  }, [extent]);

  return (
    <div ref={mapElement} className="map-container w-full h-full relative">
      <div className="absolute z-10 top-3 left-0 right-0 flex justify-center pointer-events-none">
        <div className="bg-white bg-opacity-80 px-2 py-1 text-xs flex items-center">
          <BiInfoCircle className="mr-1" />
          <span>
            Le nombre de points affichés à l'écran est limité à <strong>{size}</strong>
          </span>
        </div>
      </div>
      <div ref={tooltipElement} className="bg-black bg-opacity-80 text-white py-1 px-2 shadow-xl rounded-md"></div>
      {children}
    </div>
  );
};

/**
 * ResultMap
 */
const ResultMap = (props: ResultMapProps) => {
  const { includeFields, dataField, size, children, ...hocProps } = props;

  const defaultQuery = (value: any) => {
    let query: any = { exists: { field: dataField } };
    if (value) {
      query = getQueryForExtent(dataField, parseExtent(value));
    }

    return {
      query,
      size: size,
      aggs: {}, // need it to avoid a bug (aggs from other components is injected, even from defaultQuery !)
      _source: {
        includes: includeFields ? [dataField, ...includeFields] : [".*"],
        exclude: [],
      },
    };
  };

  return (
    <ReactiveComponent
      defaultQuery={defaultQuery}
      size={size}
      URLParams={true}
      render={(p: ReactiveComponentRenderProps) => <ResultMapComponent {...props} {...p} />}
      {...hocProps}
    />
  );
};

export default ResultMap;
