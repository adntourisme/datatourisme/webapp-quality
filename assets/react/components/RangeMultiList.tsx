/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

import { useCallback } from "react";
import MultiList, { MultiListProps } from "./MultiList";

export type RangeMultiListProps = {
  ranges: {
    [key: string]: {
      from?: number,
      to?: number
    }
  };
} & Omit<MultiListProps, "defaultQuery" | "customQuery" | "size">;

const RangeMultiList = (props: RangeMultiListProps) => {
  const { ranges, dataField } = props;

  // inner query
  const defaultQuery = useCallback(() => ({
    aggs: {
      [dataField]: {
        range: {
          field: dataField,
          ranges: Object.entries(ranges).map((r) => ({ key: r[0], ...r[1] })),
        },
      },
    }
  }), [dataField, ranges]);

  // external query
  const customQuery = useCallback((keys: string[]) => {
    if (keys.length) {
      const must = keys.map(key => {
        const {from: gte = null, to: lt = null} = ranges[key];
        return {
          range: { [dataField]: { gte, lt } }
        }
      });

      return { query: (must.length === 1 ? must[0] : { bool: { must } }) };

    }
  }, [dataField, ranges]);

  return <MultiList
    {...props}
    defaultQuery={defaultQuery}
    customQuery={customQuery}
  />
}

export default RangeMultiList;
