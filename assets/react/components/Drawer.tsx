/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

import { useEffect } from "react";

export type DrawerProps = {
  open: boolean
  onClose?: () => void
} & React.HTMLAttributes<HTMLDivElement>;

const Drawer = (props: DrawerProps) => {
  const { open, children, onClose, className } = props;

  useEffect(() => {
    if (open) {
      // option 1
      // document.body.style.setProperty("overflow", "hidden");

      // option 2
      document.body.style.setProperty("position", "fixed");
      document.body.style.setProperty("overflow-y", "scroll");
      document.body.style.setProperty("width", "100%");
    } else {
      document.body.style.removeProperty("position");
      document.body.style.removeProperty("overflow");
    }
    return () => {
      document.body.style.removeProperty("position");
      document.body.style.removeProperty("overflow");
    }
  }, [open]);

  return <div className={`fixed top-0 right-0 left-0 bottom-0 z-[1031] bg-black ease-in-out transition-all duration-300 ${open ? 'bg-opacity-50' : 'bg-opacity-0 pointer-events-none'} `} onClick={() => { open && onClose && onClose()}}>
    <div className={`transform top-0 right-0 w-[640px] max-w-full bg-white fixed h-full overflow-auto ease-in-out transition-all duration-300 z-30 shadow-2xl ${open ? 'translate-x-0' : 'translate-x-full'} ${className}`} onClick={(e) => { e.stopPropagation(); }}>
      {children}
    </div>
  </div>
}

export default Drawer
