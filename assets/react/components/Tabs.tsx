/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

import React, { useContext } from "react";

const TabsContext = React.createContext<
  { selectedItem?: any; onChange?: (key: any) => void }
>({});

type TabsProps = {
  selectedItem?: any;
  onChange?: (key: any) => void;
} & React.HtmlHTMLAttributes<HTMLDivElement>;

export const Tabs = (props: TabsProps) => {
  const { selectedItem, onChange, children, className } = props;
  const context = { selectedItem, onChange };
  return (
    <TabsContext.Provider value={context}>
      <div className={`flex flex-col overflow-auto ${className || ''}`}>
        {children}
      </div>
    </TabsContext.Provider>
  );
};

type TabProps = {
  item: any
} & React.HtmlHTMLAttributes<HTMLButtonElement>;

export const Tab = (props: TabProps) => {
  const { item, children, className } = props;
  const { selectedItem, onChange } = useContext(TabsContext);
  const active = (selectedItem === item);
  const onClick = () => {
    onChange && onChange(item)
  }

  const _className = `${className || ''} ${active ? 'text-dge-blue border-dge-blue active' : 'text-gray-500 border-transparent hover:text-gray-600 hover:border-gray-300'}`.trim();

  return (
      <button className={`inline-block font-medium text-center border-b-2 mr-2 ${_className}`} onClick={onClick}>
        {children}
      </button>
  );
};

type TabListProps = {
  children: React.ReactElement<TabProps>[];
} & React.HtmlHTMLAttributes<HTMLDivElement>;

export const TabList = (props: TabListProps) => {
  const { children, className } = props;
  return (
    <div className="border-b border-gray-200">
      <div className={`flex flex-wrap -mb-px ${className || ''}`}>{children}</div>
    </div>
  );
};

type TabPanelProps = {
  item: any
} & React.HtmlHTMLAttributes<HTMLDivElement>;

export const TabPanel = (props: TabPanelProps) => {
  const { item, children } = props;
  const context = useContext(TabsContext);


  return context?.selectedItem === item ? <div className="flex-1 overflow-auto h-full">{children}</div> : <></>;
}
