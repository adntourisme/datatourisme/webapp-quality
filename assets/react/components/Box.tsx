/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

import React, { PropsWithChildren } from "react";

type BoxProps = {
  title?: string;
  icon?: string;
  active?: boolean;
} & PropsWithChildren<React.HTMLAttributes<HTMLDivElement>>;

const Box = React.forwardRef<HTMLDivElement, BoxProps>((props: BoxProps, ref) => {
  const { icon, title = null, active = false, children } = props;
  return (
    <div
      ref={ref}
      className={`box box-${
        active ? "primary" : "default"
      } box-sm box-solid box-poi-type m-0 mb-4`}
    >
      {title && (
        <div className="box-header with-border">
          <h4 className="box-title">
            {icon && <span className={icon} />} {title}
          </h4>
        </div>
      )}
      <div className="box-body p-0">{children}</div>
    </div>
  );
});

export default Box;
