/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

import { ResponsiveLine } from "@nivo/line";
import moment from "moment";
import { useContext, useEffect, useState } from "react";
import { WidgetInnerProps } from "../../interfaces";
import { format } from "../../utils/number";
import { analyzers } from "../../variables";
import { DashboardContext } from "../Dashboard";

export const AnomalyEvolutionWidgetInner = (props: WidgetInnerProps) => {
  const { setLoading } = props;
  const { client, theme, colors } = useContext(DashboardContext);
  const [data, setData] = useState<any[]>([]);

  const processBuckets = (data) => {
    const obj: { [key: string]: any } = {};

    data.forEach((b: any) => {
      const x = moment(b.key).format("YYYY-MM-DD");
      b.anomalies.analyzer.buckets.forEach((b2: any) => {
        obj[b2.key] = obj[b2.key] || [];
        obj[b2.key].push({ x, y: b2.count.value });
      });
    });

    return Object.entries(obj).map((e) => {
      const key = e[0];
      const { label, color } = analyzers[key];
      return { id: label || key, data: e[1], color };
    });
  };

  const date_histogram = {
    field: "date",
    calendar_interval: "1d",
    time_zone: "Europe/Paris",
    min_doc_count: 1,
  };

  const query = {
    bool: {
      filter: {
        range: {
          date: {
            gte: "now-1M",
          },
        },
      },
    },
  };

  useEffect(() => {
    setLoading(true);
    client
      .search("statistics", {
        size: 0,
        body: {
          query,
          aggs: {
            date: {
              date_histogram,
              aggs: {
                anomalies: {
                  nested: {
                    path: "anomalies",
                  },
                  aggs: {
                    analyzer: {
                      terms: {
                        field: "anomalies.analyzer",
                      },
                      aggs: {
                        count: {
                          sum: {
                            field: "anomalies.count",
                          },
                        },
                        // objectCount: {
                        //   sum: {
                        //     field: "anomalies.objectCount",
                        //   },
                        // },
                      },
                    },
                  },
                },
              },
            },
          },
        },
      })
      .then((data) => {
        setData(processBuckets(data.aggregations.date.buckets));
        setLoading(false);
      });
  }, []);

  return (
    <ResponsiveLine
      margin={{ top: 20, right: 20, bottom: 40, left: 40 }}
      data={data}
      theme={theme}
      colors={{ datum: "color" }}
      xScale={{ type: "time", min: "auto", max: "auto", format: "%Y-%m-%d", precision: "day" }}
      xFormat="time:%d/%m"
      yScale={{ type: "linear", min: "auto", max: "auto", stacked: false, reverse: false }}
      yFormat={(v: any) => format(v)}
      //curve="stepBefore"
      curve="natural"
      axisLeft={{
        format: "~s",
      }}
      axisTop={null}
      axisRight={null}
      axisBottom={{
        format: "%d %b",
        tickValues: "every week",
      }}
      pointSize={6}
      pointBorderWidth={1}
      pointBorderColor={{ from: "serieColor" }}
      pointColor="#FFF"
      enablePointLabel={false}
      enableGridX={false}
      useMesh={true}
      enableSlices={"x"}
      // legends={[
      //   {
      //     anchor: "bottom",
      //     direction: "row",
      //     justify: false,
      //     translateX: 0,
      //     translateY: 50,
      //     itemsSpacing: 0,
      //     itemDirection: "left-to-right",
      //     itemWidth: 160,
      //     itemHeight: 20,
      //     itemOpacity: 1,
      //     symbolSize: 18,
      //     symbolShape: "square",
      //   },
      // ]}
    />
  );
};

const AnomalyEvolutionWidget = () => {
  const [loading, setLoading] = useState(true);

  return (
    <div className={`widget ${loading ? "loading" : ""}`}>
      <div className="box box-default drop-shadow-md">
        <div className="box-header with-border">
          <h3 className="box-title">Évolution de vos anomalies</h3>
          {/* <div className="box-tools pull-right">
            <a href="/processes" className="btn btn-box-tool"><i className="fa fa-mail-forward"></i> Historique des traitements</a>
        </div> */}
        </div>
        <div className="box-body p-0 h-[280px]">
          <AnomalyEvolutionWidgetInner setLoading={setLoading} />
        </div>
      </div>
    </div>
  );
};

export default AnomalyEvolutionWidget;
