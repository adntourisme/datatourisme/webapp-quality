/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

import { ResponsiveLine } from "@nivo/line";
import moment from "moment";
import { useContext, useEffect, useState } from "react";
import { WidgetInnerProps } from "../../interfaces";
import { format } from "../../utils/number";
import { DashboardContext } from "../Dashboard";
import { objectCountCategories } from "../variables";

export const POIEvolutionWidgetInner = (props: WidgetInnerProps) => {
  const { setLoading } = props;
  const { client, theme, colors } = useContext(DashboardContext);
  const [data, setData] = useState<any[]>([]);

  const processBuckets = (data) => {
    const obj = { abnormalObjectCount: [], objectCount: [] };

    data.forEach((b: any) => {
      const x = moment(b.key).format("YYYY-MM-DD");
      obj.objectCount.push({ x, y: b.objectCount.value });
      obj.abnormalObjectCount.push({ x, y: b.abnormalObjectCount.value });
    });

    return Object.entries(obj).map((e) => {
      const key = e[0];
      const { label: id, color, area } = objectCountCategories[key];
      return { id, data: e[1], color, area };
    });
  };

  const date_histogram = {
    field: "date",
    calendar_interval: "1d",
    time_zone: "Europe/Paris",
    min_doc_count: 1,
  };

  const query = {
    bool: {
      filter: {
        range: {
          date: {
            gte: "now-1M",
          },
        },
      },
    },
  };

  useEffect(() => {
    setLoading(true);
    client
      .search("statistics", {
        size: 0,
        body: {
          query,
          aggs: {
            date: {
              date_histogram,
              aggs: {
                objectCount: {
                  sum: {
                    field: "objectCount",
                  },
                },
                abnormalObjectCount: {
                  sum: {
                    field: "abnormalObjectCount",
                  },
                },
              },
            },
          },
        },
      })
      .then((data) => {
        setData(processBuckets(data.aggregations.date.buckets));
        setLoading(false);
      });
  }, []);

  // @see https://github.com/plouc/nivo/issues/847
  const Areas: any = ({ series, areaGenerator, areaOpacity, xScale, yScale }) => {
    const computed = series.slice(0).reverse();
    return computed.map(({ id, data, color, area }) => (
      <path
        key={id}
        d={areaGenerator(data.map((d) => ({ x: xScale(d.data.x), y: yScale(d.data.y) })))}
        fill={area ? color : "none"}
        fillOpacity={areaOpacity}
      />
    ));
  };

  return (
    <ResponsiveLine
      margin={{ top: 20, right: 20, bottom: 60, left: 40 }}
      data={data}
      theme={theme}
      colors={{ datum: "color" }}
      xScale={{ type: "time", min: "auto", max: "auto", format: "%Y-%m-%d", precision: "day" }}
      xFormat="time:%d/%m"
      yScale={{ type: "linear", min: 0, max: "auto", stacked: false, reverse: false }}
      yFormat={format}
      //curve="stepBefore"
      curve="natural"
      areaOpacity={1}
      axisLeft={{
        format: "~s",
      }}
      axisTop={null}
      axisRight={null}
      axisBottom={{
        format: "%d %b",
        tickValues: "every week",
      }}
      pointSize={6}
      pointBorderWidth={1}
      pointBorderColor={{ from: "serieColor" }}
      pointColor="#FFF"
      enablePointLabel={false}
      enableGridX={false}
      useMesh={true}
      enableSlices={"x"}
      legends={[
        {
          anchor: "bottom",
          direction: "row",
          justify: false,
          translateX: 0,
          translateY: 50,
          itemsSpacing: 0,
          itemDirection: "left-to-right",
          itemWidth: 160,
          itemHeight: 20,
          itemOpacity: 1,
          symbolSize: 18,
          symbolShape: "square",
        },
      ]}
      layers={["grid", "markers", "axes", Areas, "crosshair", "lines", "points", "slices", "mesh", "legends"]}
    />
  );
};

const POIEvolutionWidget = () => {
  const [loading, setLoading] = useState(true);

  return (
    <div className={`widget ${loading ? "loading" : ""}`}>
      <div className="box box-default drop-shadow-md">
        <div className="box-header with-border">
          <h3 className="box-title">Évolution de vos POI</h3>
        </div>
        <div className="box-body p-0 h-[280px]">
          <POIEvolutionWidgetInner setLoading={setLoading} />
        </div>
      </div>
    </div>
  );
};

export default POIEvolutionWidget;
