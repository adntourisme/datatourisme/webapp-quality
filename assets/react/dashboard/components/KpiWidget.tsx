/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

import moment from "moment";
import { useContext, useEffect, useState } from "react";
import { Sparklines, SparklinesLine } from "react-sparklines";
import { SearchTotalHits } from "../../utils/es-client";
import { format } from "../../utils/number";
import { DashboardContext } from "../Dashboard";

const KpiWidget = () => {
  const { client } = useContext(DashboardContext);
  const [loading, setLoading] = useState(true);

  const [globalObjectCount, setGlobalObjectCount] = useState(null);
  const [objectCount, setObjectCount] = useState(null);
  const [abnormalObjectCount, setAbnormalObjectCount] = useState(null);
  const [cityCount, setCityCount] = useState(null);
  const [illustratedObjectCount, setIllustratedObjectCount] = useState(null);

  const [globalObjectCountEvolution, setGlobalObjectCountEvolution] = useState([]);
  const [objectCountEvolution, setObjectCountEvolution] = useState([]);
  const [abnormalObjectPercentEvolution, setAbnormalObjectPercentEvolution] = useState([]);

  useEffect(() => {
    const promises = [];

    const query = {
      bool: {
        filter: {
          range: {
            date: {
              gte: "now-1M",
            },
          },
        },
      },
    };

    const date_histogram = {
      field: "date",
      calendar_interval: "1d",
      time_zone: "Europe/Paris",
      min_doc_count: 1,
    };

    promises.push(
      client.search("datatourisme", { size: 0, track_total_hits: true }, { params: { all: 1 } }).then((data) => {
        setGlobalObjectCount((data.hits.total as SearchTotalHits).value);
      })
    );

    promises.push(
      client
        .search("datatourisme", {
          size: 0,
          track_total_hits: true,
          body: {
            query: { match_all: {} },
            aggs: {
              abnormal: {
                filter: {
                  range: { "analyze.anomalyStatistics.total": { gt: 0 } },
                },
              },
              city_count: {
                cardinality: {
                  field: "isLocatedAt.address.hasAddressCity.insee",
                },
              },
              has_media: {
                filter: { range: { "analyze.mediaCount": { gt: 0 } } },
              },
            },
          },
        })
        .then((data) => {
          setObjectCount((data.hits.total as SearchTotalHits).value);
          setAbnormalObjectCount(data.aggregations.abnormal.doc_count);
          setCityCount(data.aggregations.city_count.value);
          setIllustratedObjectCount(data.aggregations.has_media.doc_count);
        })
    );

    promises.push(
      client
        .search(
          "statistics",
          {
            size: 0,
            body: {
              query,
              aggs: {
                date: {
                  date_histogram,
                  aggs: {
                    objectCount: {
                      sum: {
                        field: "objectCount",
                      },
                    },
                  },
                },
              },
            },
          },
          { params: { all: 1 } }
        )
        .then((data) => {
          setGlobalObjectCountEvolution(
            data.aggregations.date.buckets.map((s: any) => {
              return s.objectCount.value;
            })
          );
        })
    );

    promises.push(
      client
        .search("statistics", {
          size: 0,
          body: {
            query,
            aggs: {
              date: {
                date_histogram,
                aggs: {
                  objectCount: {
                    sum: {
                      field: "objectCount",
                    },
                  },
                  abnormalObjectCount: {
                    sum: {
                      field: "abnormalObjectCount",
                    },
                  },
                },
              },
            },
          },
        })
        .then((data) => {
          setObjectCountEvolution(
            data.aggregations.date.buckets.map((s: any) => {
              return s.objectCount.value;
            })
          );
          setAbnormalObjectPercentEvolution(
            data.aggregations.date.buckets.map((s: any) => {
              return s.abnormalObjectCount.value / s.objectCount.value;
            })
          );
        })
    );

    Promise.all(promises).then(() => {
      setLoading(false);
    });
  }, []);

  const abnormalPercent =
    objectCount && abnormalObjectCount ? Math.round((abnormalObjectCount * 100) / objectCount) : null;
  const illustratedPercent =
    objectCount && illustratedObjectCount ? Math.round((illustratedObjectCount * 100) / objectCount) : null;

  let abnormalBoxBg = ["from-green-500", "to-green-700"];
  if (abnormalPercent > 30) {
    abnormalBoxBg = ["from-yellow-500", "to-yellow-700"];
  }
  if (abnormalPercent > 50) {
    abnormalBoxBg = ["from-orange-500", "to-orange-700"];
  }
  if (abnormalPercent > 75) {
    abnormalBoxBg = ["from-red-500", "to-red-700"];
  }

  return (
    <div className={`widget-kpis grid grid-cols-2 gap-4 ${loading ? "loading" : ""}`}>
      <div className="small-box mb-0 bg-gradient-to-br from-sky-500 to-sky-700 text-white drop-shadow-md">
        {!loading && (
          <>
            <div className="inner">
              <div>Total de l'ensemble des POI</div>
              <h3>{globalObjectCount && format(globalObjectCount)}</h3>
            </div>
            <Sparklines data={globalObjectCountEvolution} height={30} limit={30}>
              <SparklinesLine style={{ strokeWidth: 3, stroke: "#FFFFFF", fill: "none" }} />
            </Sparklines>
          </>
        )}
      </div>
      <div className="small-box mb-0 bg-gradient-to-br from-teal-500 to-teal-700 text-white drop-shadow-md">
        {!loading && (
          <>
            <div className="inner">
              <div>Total de vos POI</div>
              <h3>{objectCount && format(objectCount)}</h3>
            </div>
            <Sparklines data={objectCountEvolution} height={30} limit={30}>
              <SparklinesLine style={{ strokeWidth: 3, stroke: "#FFFFFF", fill: "none" }} />
            </Sparklines>
          </>
        )}
      </div>
      <div className={`small-box mb-0 bg-gradient-to-br text-white drop-shadow-md ${abnormalBoxBg.join(" ")}`}>
        {!loading && (
          <>
            <div className="inner">
              <div>Taux de vos POI en erreur</div>
              {abnormalPercent && (
                <h3 title={abnormalObjectCount}>
                  {abnormalPercent} <sup style={{ fontSize: "50%" }}>%</sup>
                </h3>
              )}
            </div>
            <Sparklines data={abnormalObjectPercentEvolution} height={30} limit={30}>
              <SparklinesLine style={{ strokeWidth: 3, stroke: "#FFFFFF", fill: "none" }} />
            </Sparklines>
          </>
        )}
      </div>
      <div className="small-box mb-0 bg-gradient-to-br from-stone-500 to-stone-700 text-white drop-shadow-md">
        {!loading && (
          <div className="inner flex flex-col h-full justify-between">
            <div>
              <div>Communes couvertes</div>
              <h3 className="text-2xl leading-none">{cityCount && format(cityCount)}</h3>
            </div>
            <div>
              <div>POI avec au moins un média</div>
              {illustratedPercent && (
                <h3 className="text-2xl leading-none" title={illustratedObjectCount}>
                  {illustratedPercent} <sup style={{ fontSize: "50%" }}>%</sup>
                </h3>
              )}
            </div>
          </div>
        )}
      </div>
    </div>
  );
};

export default KpiWidget;
