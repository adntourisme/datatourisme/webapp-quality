/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

import { Theme } from "@nivo/core";
import React from "react";
import ElasticsearchClient from "../utils/es-client";
import AnomalyEvolutionWidget from "./components/AnomalyEvolutionWidget";
import AnomalyRepartitionWidget from "./components/AnomalyRepartitionWidget";
import KpiWidget from "./components/KpiWidget";
import LeaderBoardWidget from "./components/LeaderBoardWidget";
import MapWidget from "./components/MapWidget";
import POIEvolutionWidget from "./components/POIEvolutionWidget";
import POIRepartitionWidget from "./components/POIRepartitionWidget";
import { colors } from "../variables";
import NewsWidget from "./components/NewsWidget";
import POIWidget from "./components/POIWidget";
import AnomalyWidget from "./components/AnomalyWidget";

export type DashboardContextParams = {
  client: ElasticsearchClient
  theme: Theme
  colors: string[]
  producers: {[key: number]: string}
};

const theme: Theme = {
  fontFamily: '"Source Sans Pro", "Helvetica Neue", Helvetica, Arial, sans-serif',
  fontSize: 12,
  textColor: '#333'
}

export const DashboardContext =
  React.createContext<DashboardContextParams>(null);


const Dashboard = ({ url }: { url: string }) => {
  const client = new ElasticsearchClient(url);
  const producers = (window as any).PRODUCERS || {};

  return (
    <DashboardContext.Provider value={{ client, theme, colors, producers }}>
      <section className="content dashboard">
        <div className="grid grid-cols-1 md:grid-cols-2 gap-4">
          <div><KpiWidget /></div>
          <div><NewsWidget /></div>
          <div><MapWidget /></div>
          <div><POIWidget /></div>
          <div><AnomalyWidget /></div>
          <div><LeaderBoardWidget /></div>
        </div>
      </section>
    </DashboardContext.Provider>
  );
};

export default Dashboard;
