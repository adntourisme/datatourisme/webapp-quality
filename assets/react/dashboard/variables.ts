/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

import { colors } from "../variables";

export const objectCountCategories: {[key: string]: {label: string, color: string, area: boolean}} = {
  objectCount: {
    label: "Total de vos POI",
    color: colors[0],
    area: false
  },
  abnormalObjectCount: {
    label: "Total de vos POI en erreur",
    color: '#dc2626',
    area: true
  }
}
