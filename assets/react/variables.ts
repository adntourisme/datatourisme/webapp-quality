/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

import { IconType } from "react-icons";
import { BiImage, BiLink, BiListCheck, BiMapPin, BiSpreadsheet } from "react-icons/bi";

export const colors: string[] = [
  "#0284c7",
  "#f97316",
  "#65a30d",
  "#facc15",
  "#76b7b2",
  "#a855f7",
  "#fda4af",
  "#9c755f",
  "#bab0ab",
  "#5a5a5a"
];

export const analyzers: {[key: string]: {label: string, icon: IconType, color: string}} = {
  MediaAnalyzer: {
    label: "Anomalie de média",
    icon: BiImage,
    color: colors[0]
  },
  TextAnalyzer: {
    label: "Anomalie de contenu",
    icon: BiSpreadsheet,
    color: colors[1]
  },
  GeoCoordinatesAnalyzer: {
    label: "Anomalie géospatiale",
    icon: BiMapPin,
    color: colors[2]
  },
  ConsistencyAnalyzer: {
    label: "Anomalie de cohérence",
    icon: BiListCheck,
    color: colors[3]
  },
  LinksAnalyzer: {
    label: "Anomalie de lien web",
    icon: BiLink,
    color: colors[4]
  },
};

export const analyzersLabel = Object.fromEntries(Object.entries(analyzers).map(([key, { label }]) => [key, label]));

export const types: {[key: string]: {label: string, color: string}} = {
  PlaceOfInterest: {
    label: "Lieu",
    color: colors[0]
  },
  EntertainmentAndEvent: {
    label: "Fête et manifestation",
    color: colors[1]
  },
  Product: {
    label: "Produit",
    color: colors[2]
  },
  Tour: {
    label: "Itinéraire touristique",
    color: colors[3]
  }
};

export const typesByLabel = Object.fromEntries(Object.entries(types).map(([key, { label, ...rest }]) => [label, { key, ...rest }]));
